package com.name.business.utils.exeptions;

/**
 * Created by luis on 1/10/16.
 */
public class TechnicalException implements IException {
    private String message;

    public TechnicalException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
