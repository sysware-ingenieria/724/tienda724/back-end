package com.name.business.utils.exeptions;

import fj.data.Either;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Permite crear Error personalizados, Ya sea  de aplicacion o de Servidor
 */
public class ExceptionResponse {
    public static Response createErrorResponse(Either<IException,?> iExceptionEither){
        IException iException= iExceptionEither.left().value();

        Status statusCode=iException instanceof BussinessException ? Status.BAD_REQUEST:Status.INTERNAL_SERVER_ERROR;
        return Response.status(statusCode).entity(iException).build();

    }

}
