package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.AttributeValueBusiness;
import com.name.business.entities.AttributeList;
import com.name.business.entities.AttributeValue;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.representations.AttributeDTO;
import com.name.business.representations.AttributeValueDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/attributes/values")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AttributeValueResource {
    private AttributeValueBusiness attributeValueBusiness;

    public AttributeValueResource(AttributeValueBusiness attributeValueBusiness) {
        this.attributeValueBusiness = attributeValueBusiness;
    }
    @GET
    @Timed
    public Response getAttributeValueResourceList(
            @QueryParam("id_attribute_value") Long id_attribute_value,
            @QueryParam("id_attribute") Long id_attribute,
            @QueryParam("id_common_attrib_value") Long id_common_attrib_value,
            @QueryParam("name_attrib_value") String name_attrib_value,
            @QueryParam("description_attrib_value") String description_attrib_value,
            @QueryParam("id_state_attrib_value") Long id_state_attrib_value,
            @QueryParam("state_attrib_value") Integer state_attrib_value,
            @QueryParam("creation_attrib_value") Date creation_attrib_value,
            @QueryParam("modify_attrib_value") Date modify_attrib_value) {
        Response response;

        Either<IException, List<AttributeValue>> getAttributes = attributeValueBusiness.getAttributeValues(
                new AttributeValue(id_attribute_value,id_attribute,
                        new Common(
                                id_common_attrib_value,
                                name_attrib_value,
                                description_attrib_value
                        ),
                        new CommonState(id_state_attrib_value, state_attrib_value,
                                creation_attrib_value, modify_attrib_value)
                )
        );

        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }

    /**
     *
     * @param attributeValueDTO
     * @return response
     */
    @POST
    @Timed
    public Response postAttributeValueResource(AttributeValueDTO attributeValueDTO){
        Response response;

        Either<IException, Long> attributeValueEither = attributeValueBusiness.createAttributeValue(attributeValueDTO);

        if (attributeValueEither.isRight()){

            response=Response.status(Response.Status.OK).entity(attributeValueEither.right().value()).build();

        }else {
            response= ExceptionResponse.createErrorResponse(attributeValueEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateAttributeValuesResource(@PathParam("id") Long id_attribute, AttributeValueDTO attributeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeValueBusiness.updateAttributeValue(id_attribute, attributeDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_attribute_value){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeValueBusiness.deleteAttributeValue(id_attribute_value);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
