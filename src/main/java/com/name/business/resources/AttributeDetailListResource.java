package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.AttributeDetailListBusiness;
import com.name.business.entities.*;
import com.name.business.representations.AttributeDetailListDTO;
import com.name.business.representations.AttributeListDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/attributes/list/deatils")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AttributeDetailListResource {
    private AttributeDetailListBusiness attributeDetailListBusiness;

    public AttributeDetailListResource(AttributeDetailListBusiness attributeDetailListBusiness) {
        this.attributeDetailListBusiness = attributeDetailListBusiness;
    }

    @GET
    @Timed
    public Response getAttributeDeatilListResourceList(@QueryParam("id_attribute_detail_list") Long id_attribute_detail_list,
                                                       @QueryParam("id_attribute_list") Long id_attribute_list,
                                                 @QueryParam("id_state_attrib_detail_list")Long id_state_attrib_detail_list,
                                                 @QueryParam("state_attrib_detail_list") Integer state_attrib_detail_list,
                                                 @QueryParam("creation_attrib_detail_list") Date creation_attrib_detail_list,
                                                 @QueryParam("modify_attrib_detail_list") Date modify_attrib_detail_list,

                                                       @QueryParam("id_attribute_value") Long id_attribute_value,
                                                       @QueryParam("id_attribute") Long id_attribute,
                                                       @QueryParam("id_common_attrib_value") Long id_common_attrib_value,
                                                       @QueryParam("name_attrib_value") String name_attrib_value,
                                                       @QueryParam("description_attrib_value") String description_attrib_value,
                                                       @QueryParam("id_state_attrib_value") Long id_state_attrib_value,
                                                       @QueryParam("state_attrib_value") Integer state_attrib_value,
                                                       @QueryParam("creation_attrib_value") Date creation_attrib_value,
                                                       @QueryParam("modify_attrib_value") Date modify_attrib_value) {
        Response response;

        Either<IException, List<AttributeDetailList>> getAttributes = attributeDetailListBusiness.getAttributeDetailLists(
                new AttributeDetailList(id_attribute_detail_list,
                        id_attribute_list,
                        new CommonState(id_state_attrib_detail_list, state_attrib_detail_list,
                                creation_attrib_detail_list, modify_attrib_detail_list),
                        new AttributeValue(
                                id_attribute_value,
                                id_attribute,
                                new Common(
                                        id_common_attrib_value,
                                        name_attrib_value,
                                        description_attrib_value
                                ),
                                new CommonState(
                                        id_state_attrib_value,
                                        state_attrib_value,
                                        creation_attrib_value,
                                        modify_attrib_value
                                )
                        )
                        )
                );


        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }



    /**
     *
     * @param attributeDetailListDTOS
     * @return status
     */
    @POST
    @Timed
    public Response postAttributeResource(@QueryParam("id_attribute_list")Long id_attribute_list, List<AttributeDetailListDTO> attributeDetailListDTOS){
        Response response;

        Either<IException, Long> mailEither = attributeDetailListBusiness.createAttributeDetailList(id_attribute_list,attributeDetailListDTOS);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @Path("/{id}")
    @PUT
    @Timed
    public Response updateAttributeListResource(@PathParam("id") Long id_attribute_detail_list,@QueryParam("id_attribute_list") Long id_attribute_list, AttributeDetailListDTO attributeDetailListDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeDetailListBusiness.updateAttributeDetailList(id_attribute_list,id_attribute_list, attributeDetailListDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_attribute_detail_list){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeDetailListBusiness.deleteAttributeDetail(id_attribute_detail_list);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
