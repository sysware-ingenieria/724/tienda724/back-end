package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.TaxTariffBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.TaxTariff;
import com.name.business.representations.PriceListDTO;
import com.name.business.representations.ProductDTO;
import com.name.business.representations.TaxTariffDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/tax-tariff")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TaxTariffResource {

    private TaxTariffBusiness taxTariffBusiness;

    public TaxTariffResource(TaxTariffBusiness taxTariffBusiness) {
        this.taxTariffBusiness = taxTariffBusiness;
    }

    @GET
    @Timed
    public Response getTaxTariff(

            @QueryParam("id_tax_tariff") Long id_tax_tariff,
            @QueryParam("percent") Double percent,
            @QueryParam("id_common_tax") Long id_common_tax,
            @QueryParam("name_tax") String name_tax,
            @QueryParam("description_tax") String description_tax,
            @QueryParam("id_state_tax") Long id_state_tax,
            @QueryParam("state_tax") Integer state_tax,
            @QueryParam("creation_tax") Date creation_tax,
            @QueryParam("modify_tax") Date modify_tax)

    {
        Response response;

        Either<IException, List<TaxTariff>> getProducts = taxTariffBusiness.getProduct(
                new TaxTariff(id_tax_tariff, percent,
                        new Common(
                                id_common_tax,
                                name_tax,
                                description_tax
                        ),
                        new CommonState(id_state_tax, state_tax,
                                creation_tax, modify_tax)
                )
        );

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value().size());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_product){
        Response response;
        Either<IException, Long> allViewOffertsEither = taxTariffBusiness.deleteProduct(id_product);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateProductResource(@PathParam("id") Long id_product, TaxTariffDTO taxTariffDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = taxTariffBusiness.updateTaxtTariff(id_product, taxTariffDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param taxTariffDTO
     * @return status
     */
    @POST
    @Timed
    public Response postProductResource(TaxTariffDTO taxTariffDTO){
        Response response;

        Either<IException, Long> mailEither = taxTariffBusiness.createProduct(taxTariffDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



}

