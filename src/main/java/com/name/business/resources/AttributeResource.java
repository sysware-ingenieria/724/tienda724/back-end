package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.AttributeBusiness;
import com.name.business.entities.Attribute;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.representations.AttributeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/attributes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AttributeResource {

    private AttributeBusiness attributeBusiness;

    public AttributeResource(AttributeBusiness attributeBusiness) {
        this.attributeBusiness = attributeBusiness;
    }

    @GET
    @Timed
    public Response getAttributeResourceList(@QueryParam("id_attribute") Long id_attribute,@QueryParam("id_common") Long id_common,
                                         @QueryParam("name") String name,@QueryParam("description") String description,
                                         @QueryParam("id_state")Long id_state,@QueryParam("state") Integer state,
                                         @QueryParam("creation_attribute") Date creation_attribute,
                                         @QueryParam("modify_attribute") Date modify_attribute) {
        Response response;

        Either<IException, List<Attribute>> getAttributes = attributeBusiness.getAttributes(
                                                                                new Attribute(id_attribute,
                                                                                new Common(id_common, name, description),
                                                                                new CommonState(id_state, state,
                                                                                        creation_attribute, modify_attribute)
                                                                                )
                                                                );
        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }
    /**
     *
     * @param attributeDTO
     * @return status
     */
    @POST
    @Timed
    public Response postAttributeResource(AttributeDTO attributeDTO){
        Response response;

        Either<IException, Long> mailEither = attributeBusiness.createAttribute(attributeDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateAttributeResource(@PathParam("id") Long id_attribute, AttributeDTO attributeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeBusiness.updateAttribute(id_attribute, attributeDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_attribute){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeBusiness.deleteAttribute(id_attribute);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
