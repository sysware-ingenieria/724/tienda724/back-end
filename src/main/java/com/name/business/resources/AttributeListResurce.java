package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.AttributeListBusiness;
import com.name.business.entities.Attribute;
import com.name.business.entities.AttributeList;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.representations.AttributeListDTO;
import com.name.business.representations.AttributeValueDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/attributes/list")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AttributeListResurce {
    private AttributeListBusiness attributeListBusiness;

    public AttributeListResurce(AttributeListBusiness attributeListBusiness) {
        this.attributeListBusiness = attributeListBusiness;
    }

    @GET
    @Timed
    public Response getAttributeListResourceList(@QueryParam("id_attribute_list") Long id_attribute_list,
                                             @QueryParam("id_common_state")Long id_common_state,
                                                 @QueryParam("state") Integer state,
                                             @QueryParam("creation_attribute") Date creation_attribute_list,
                                             @QueryParam("modify_attribute") Date modify_attribute_list) {
        Response response;

        Either<IException, List<AttributeList>> getAttributes = attributeListBusiness.getAttributeLists(
                new AttributeList(id_attribute_list,
                        new CommonState(id_common_state, state,
                                creation_attribute_list, modify_attribute_list)
                )
        );

        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }



    /**
     *
     * @param attributeListDTO
     * @return status
     */
    @POST
    @Timed
    public Response postAttributeResource(AttributeListDTO attributeListDTO){
        Response response;

        Either<IException, Long> mailEither = attributeListBusiness.createAttributeList(attributeListDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateAttributeListResource(@PathParam("id") Long id_attribute_list, AttributeListDTO attributeListDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeListBusiness.updateAttributeList(id_attribute_list, attributeListDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_attribute_list){
        Response response;
        Either<IException, Long> allViewOffertsEither = attributeListBusiness.deleteAttributeList(id_attribute_list);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}
