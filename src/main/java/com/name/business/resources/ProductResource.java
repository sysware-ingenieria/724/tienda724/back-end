package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ProductBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.Product;
import com.name.business.representations.ProductDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {
    private ProductBusiness productBusiness;

    public ProductResource(ProductBusiness productBusiness) {
        this.productBusiness = productBusiness;
    }

    /**
     *
     * @param productDTO
     * @return status
     */
    @POST
    @Timed
    public Response postProductResource(ProductDTO productDTO){
        Response response;

        Either<IException, Long> mailEither = productBusiness.createProduct(productDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @GET
    @Timed
    public Response getProduct(
            @QueryParam("id_product") Long id_product,
            @QueryParam("id_category") Long id_category,
            @QueryParam("stock") Integer stock,
            @QueryParam("stock_min") Integer stock_min,
            @QueryParam("img_url") String img_url,
            @QueryParam("code") String code,
            @QueryParam("id_tax") Long id_tax,
            @QueryParam("id_common product") Long id_common_product,
            @QueryParam("name_product") String name_product,
            @QueryParam("description_product") String description_product,

            @QueryParam("id_state_product") Long id_state_product,
            @QueryParam("state_product") Integer state_product,
            @QueryParam("creation_product") Date creation_product,
            @QueryParam("modify_product") Date modify_product)
    {
        Response response;

        Either<IException, List<Product>> getProducts = productBusiness.getProduct(
                new Product(id_product, id_category, stock, stock_min, img_url,
                        code, id_tax,
                        new Common(
                                id_common_product,
                                name_product,
                                description_product
                        ),
                        new CommonState(id_state_product, state_product,
                                creation_product, modify_product)
                )
        );

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value().size());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_product){
        Response response;
        Either<IException, Long> allViewOffertsEither = productBusiness.deleteProduct(id_product);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateProductResource(@PathParam("id") Long id_product, ProductDTO productDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = productBusiness.updateProduct(id_product, productDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


}
