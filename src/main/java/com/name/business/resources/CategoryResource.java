package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.CategoryBusiness;
import com.name.business.entities.Category;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.representations.CategoryDTO;
import com.name.business.representations.ProductDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoryResource {

    private CategoryBusiness categoryBusiness;

    public CategoryResource(CategoryBusiness categoryBusiness) {
        this.categoryBusiness = categoryBusiness;
    }

    @GET
    @Timed
    public Response getCategories(@QueryParam("id_category") Long id_category, @QueryParam("id_common") Long id_common,
                                  @QueryParam("id_category_father") Long id_category_father, @QueryParam("img_url") String img_url,
                                  @QueryParam("name") String name, @QueryParam("description") String description,
                                  @QueryParam("id_third_category") Long id_third_category,
                                  @QueryParam("id_state") Long id_state, @QueryParam("state") Integer state,
                                  @QueryParam("creation_attribute") Date creation_attribute,
                                  @QueryParam("modify_attribute") Date modify_attribute) {
        Response response;

        Either<IException, List<Category>> getCategoriesEither = categoryBusiness.
                getCategories1(

                        new Category(id_category,
                                new Common(id_common, name, description),
                                img_url, id_category_father,id_third_category,
                                new CommonState(id_state, state,
                                        creation_attribute, modify_attribute)
                        )
                );
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }

    /**
     *
     * @param categoryDTO
     * @return status
     */
    @POST
    @Timed
    public Response postCategoryResource(CategoryDTO categoryDTO){
        Response response;

        Either<IException, Long> mailEither = categoryBusiness.createCategory(categoryDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateCategoryResource(@PathParam("id") Long id_category, CategoryDTO categoryDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = categoryBusiness.updateCategory(id_category, categoryDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonResource(@PathParam("id") Long id_category){
        Response response;
        Either<IException, Long> allViewOffertsEither = categoryBusiness.deleteCategory(id_category);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}


