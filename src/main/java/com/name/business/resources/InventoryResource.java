package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.InventoryBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.Inventory;
import com.name.business.representations.InventoryDTO;
import com.name.business.representations.InventoryQuantityDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/inventories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class InventoryResource {

    private InventoryBusiness inventoryBusiness;

    public InventoryResource(InventoryBusiness inventoryBusiness) {
        this.inventoryBusiness = inventoryBusiness;
    }

    @GET
    @Timed
    public Response getInventoriesResourceList(
            @QueryParam("id_inventory") Long id_inventory,
            @QueryParam("id_third") Long id_third,
            @QueryParam("id_common_inventory") Long id_common_inventory,
            @QueryParam("name_inventory") String name_inventory,
            @QueryParam("description_inventory") String description_inventory,
            @QueryParam("id_state_inventory") Long id_state_inventory,
            @QueryParam("state_inventory") Integer state_inventory,
            @QueryParam("creation_inventory") Date creation_inventory,
            @QueryParam("modify_inventory") Date modify_inventory) {
        Response response;

        Either<IException, List<Inventory>> getAttributes = inventoryBusiness.getInventories(
                new Inventory(id_inventory,id_third,
                        new Common(
                                id_common_inventory,
                                name_inventory,
                                description_inventory
                        ),
                        new CommonState(id_state_inventory, state_inventory,
                                creation_inventory, modify_inventory)
                )
        );

        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }
    /**
     *
     * @param inventoryDTO
     * @return status
     */
    @POST
    @Timed
    public Response postInventoryResource(InventoryDTO inventoryDTO){
        Response response;

        Either<IException, Long> mailEither = inventoryBusiness.createInventory(inventoryDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @Path("/{id}")
    @PUT
    @Timed
    public Response updateAttributeResource(@PathParam("id") Long id_inventory, InventoryDTO inventoryDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = inventoryBusiness.updateInventary(id_inventory, inventoryDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }



    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteInventoryResource(@PathParam("id") Long id_inventory){
        Response response;
        Either<IException, Long> allViewOffertsEither = inventoryBusiness.deleteInventory(id_inventory);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @PUT
    @Path("/discount/{id}")
    @Timed
    public Response discountInventoryDiscountResource(@PathParam("id") Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList){
        Response response;
        Either<IException, String> allViewOffertsEither = inventoryBusiness.discountInventary(id_inventory, inventoryQuantityDTOList);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @PUT
    @Path("/plus/{id}")
    @Timed
    public Response plusInventoryDiscountResource(@PathParam("id") Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList){
        Response response;
        Either<IException, String> allViewOffertsEither = inventoryBusiness.plusInventory(id_inventory, inventoryQuantityDTOList);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}
