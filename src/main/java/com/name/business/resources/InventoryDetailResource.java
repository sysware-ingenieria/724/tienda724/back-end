package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.InventoryDetailBusiness;
import com.name.business.entities.*;
import com.name.business.representations.InventoryDetailDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/inventories-details")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class InventoryDetailResource {
    private InventoryDetailBusiness inventoryDetailBusiness;

    public InventoryDetailResource(InventoryDetailBusiness inventoryDetailBusiness) {
        this.inventoryDetailBusiness = inventoryDetailBusiness;
    }


    @GET
    @Path("/simple")
    @Timed
    public Response getInventorySimpleResourceList(
            @QueryParam("id_inventory_detail") Long id_inventory_detail,
            @QueryParam("id_inventory") Long id_inventory,
            @QueryParam("id_product_third") Long id_product_third,
            @QueryParam("quantity") Integer quantity,
            @QueryParam("code") String code,


            @QueryParam("id_state_inv_detail") Long id_state_inv_detail,
            @QueryParam("state_inv_detail") Integer state_inv_detail,
            @QueryParam("creation_inv_detail") Date creation_inv_detail,
            @QueryParam("modify_inv_detail") Date modify_inv_detail) {
        Response response;

        Either<IException, List<InventoryDetailSimple>> getAttributes = inventoryDetailBusiness.getInventoryDetailSimple(
                new InventoryDetailSimple(id_inventory_detail,id_inventory,id_product_third,quantity,code,

                        new CommonState(id_state_inv_detail, state_inv_detail,
                                creation_inv_detail, modify_inv_detail)
                )
        );

        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }


    @GET
    @Timed
    public Response getInventoriesResourceList(
            @QueryParam("id_inventory_detail") Long id_inventory_detail,
            @QueryParam("id_inventory") Long id_inventory,
            @QueryParam("id_product_third") Long id_product_third,
            @QueryParam("quantity") Integer quantity,
            @QueryParam("code_inv") String code_inv,

            @QueryParam("id_state_inv_detail") Long id_state_inv_detail,
            @QueryParam("state_inv_detail") Integer state_inv_detail,
            @QueryParam("creation_inv_detail") Date creation_inv_detail,
            @QueryParam("modify_inv_detail") Date modify_inv_detail,

            @QueryParam("id_product") Long id_product,
            @QueryParam("id_category") Long id_category,
            @QueryParam("stock") Integer stock,
            @QueryParam("stock_min") Integer stock_min,
            @QueryParam("img_url") String img_url,
            @QueryParam("code") String code,


            @QueryParam("id_tax") Long id_tax,
            @QueryParam("id_common_product") Long id_common_product,
            @QueryParam("name_product") String name_product,
            @QueryParam("description_product") String description_product,
            @QueryParam("id_state_product") Long id_state_product,
            @QueryParam("state_product") Integer state_product,
            @QueryParam("creation_product") Date creation_product,
            @QueryParam("modify_product") Date modify_product,
            @QueryParam("id_third") Long id_third,
            @QueryParam("id_attribute_list") Long id_attribute_list,
            @QueryParam("standard_price") Double standard_price,
            @QueryParam("min_price") Double min_price,
            @QueryParam("code_pt") String code_pt,
            @QueryParam("id_category_third") Long id_category_third,
            @QueryParam("location_product") String location_pro,
            @QueryParam("id_state_prod_third") Long id_state_prod_third,
            @QueryParam("state_prod_third") Integer state_prod_third,
            @QueryParam("creation_prod_third") Date creation_prod_third,
            @QueryParam("modify_prod_third") Date modify_prod_third,
            @QueryParam("id_measure_unit") Long id_measure_unit,
            @QueryParam("id_measure_unit_father") Long id_measure_unit_father,
            @QueryParam("id_common_measure_unit") Long id_common_measure_unit,
            @QueryParam("name_measure_unit") String name_measure_unit,
            @QueryParam("description_measure_unit") String description_measure_unit,
            @QueryParam("id_state_measure_unit") Long id_state_measure_unit,
            @QueryParam("state_measure_unit") Integer state_measure_unit,
            @QueryParam("creation_measure_unit") Date creation_measure_unit,
            @QueryParam("modify_measure_unit") Date modify_measure_unit) {
        Response response;
        InventoryDetail inventoryDetail= new InventoryDetail(
                new InventoryDetailSimple(id_inventory_detail, id_inventory,id_product_third,quantity,code_inv,
                        new CommonState(id_state_inv_detail,state_inv_detail,creation_inv_detail,modify_inv_detail)
                ),
                new Product(id_product,id_category,stock,stock_min,img_url,code,id_tax,
                        new Common(id_common_product,name_product,description_product),
                        new CommonState(id_state_product,state_product,creation_product,modify_product)
                ),
                new ProductThirdSimple(id_product,id_third,standard_price,min_price,code_pt,id_category_third,location_pro,
                        new CommonState(id_state_prod_third,state_prod_third,creation_prod_third,modify_prod_third),
                        new MeasureUnit(
                                id_measure_unit,id_measure_unit_father,
                                new Common(
                                        id_common_measure_unit,name_measure_unit, description_measure_unit
                                ),
                                new CommonState(
                                        id_state_measure_unit,state_measure_unit,creation_measure_unit,modify_measure_unit
                                )
                        ),
                        id_product,id_attribute_list


                )
        );

        Either<IException, List<InventoryDetail>> getAttributes = inventoryDetailBusiness.getInventoryDetails(
                inventoryDetail
        );

        if (getAttributes.isRight()){
            System.out.println(getAttributes.right().value().size());
            response=Response.status(Response.Status.OK).entity(getAttributes.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getAttributes);
        }
        return response;
    }

    /**
     *
     * @param id_inventory
     * @return status
     */
    @POST
    @Timed
    public Response postInventoryResource(@QueryParam("id_inventory")Long id_inventory, List<InventoryDetailDTO> inventoryDetailDTOList){
        Response response;

        Either<IException, Long> mailEither = inventoryDetailBusiness.createInventoryDetail(id_inventory,inventoryDetailDTOList);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateAttributeResource(@PathParam("id") Long id_inventory, List<InventoryDetailDTO> inventoryDetailDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = inventoryDetailBusiness.updateInventaryDetail(id_inventory, inventoryDetailDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteInventoryResource(@PathParam("id") Long id_inventory_detail,@QueryParam("id_inventory") Long id_inventory){
        Response response;
        Either<IException, Long> allViewOffertsEither = inventoryDetailBusiness.deleteInventoryDetail(id_inventory_detail, id_inventory);
        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}
