package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ProductThirdBusiness;
import com.name.business.entities.*;
import com.name.business.representations.ProductThirdDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/products-third")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductThirdResource {
    private ProductThirdBusiness productThirdBusiness;

    public ProductThirdResource(ProductThirdBusiness productThirdBusiness) {
        this.productThirdBusiness = productThirdBusiness;
    }

    /**
     *
     * @param productThirdDTO
     * @return status
     */
    @POST
    @Timed
    public Response postProductResource(ProductThirdDTO productThirdDTO){
        Response response;

        Either<IException, Long> mailEither = productThirdBusiness.createProductThird(productThirdDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @GET
    @Path("/simple")
    @Timed
    public Response getProductThirdSimpleResource(
            @QueryParam("id_product_third") Long id_product_third,
            @QueryParam("id_third") Long id_third,
            @QueryParam("standard_price") Double standard_price,
            @QueryParam("min_price") Double min_price,
            @QueryParam("code") String code,
            @QueryParam("id_category_third") Long id_category_third,
            @QueryParam("location") String location,

            @QueryParam("id_state_prod_third") Long id_state_prod_third,
            @QueryParam("state_prod_third") Integer state_prod_third,
            @QueryParam("creation_prod_third") Date creation_prod_third,
            @QueryParam("modify_prod_third") Date modify_prod_third,

            @QueryParam("id_measure_unit") Long id_measure_unit,
            @QueryParam("id_measure_unit_father") Long id_measure_unit_father,
            @QueryParam("id_common_measure_unit") Long id_common_measure_unit,
            @QueryParam("name_measure_unit") String name_measure_unit,
            @QueryParam("description_measure_unit") String description_measure_unit,
            @QueryParam("id_state_measure_unit") Long id_state_measure_unit,
            @QueryParam("state") Integer state_measure_unit,
            @QueryParam("creation_measure_unit") Date creation_measure_unit,
            @QueryParam("modify_measure_unit") Date modify_measure_unit,

            @QueryParam("id_product") Long id_product,
            @QueryParam("id_attribute_list") Long id_attribute_list

    )
    {
        Response response;

        Either<IException, List<ProductThirdSimple>> getProducts = productThirdBusiness.getProductThirdSimple(
                new ProductThirdSimple(id_product_third, id_third, standard_price,
                        min_price, code,id_category_third,location,
                        new CommonState(
                                id_state_prod_third,
                                state_prod_third,
                                creation_prod_third,
                                modify_prod_third
                        ),

                        new MeasureUnit(
                                id_measure_unit,
                                id_measure_unit_father,
                                new Common(
                                        id_common_measure_unit,
                                        name_measure_unit,
                                        description_measure_unit
                                ),
                                new CommonState(
                                        id_state_measure_unit,
                                        state_measure_unit,
                                        creation_measure_unit,
                                        modify_measure_unit
                                )
                        ),

                        id_product,
                        id_attribute_list
                )
        );

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value().size());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Timed
    public Response getProductThirdResource(
            @QueryParam("id_product_third") Long id_product_third,
            @QueryParam("id_third") Long id_third,
            @QueryParam("standard_price") Double standard_price,
            @QueryParam("min_price") Double min_price,
            @QueryParam("code") String code,
            @QueryParam("id_category_third") Long id_category_third,
            @QueryParam("location") String location,

            @QueryParam("id_state_prod_third") Long id_state_prod_third,
            @QueryParam("state_prod_third") Integer state_prod_third,
            @QueryParam("creation_prod_third") Date creation_prod_third,
            @QueryParam("modify_prod_third") Date modify_prod_third,

            @QueryParam("id_measure_unit") Long id_measure_unit,
            @QueryParam("id_measure_unit_father") Long id_measure_unit_father,
            @QueryParam("id_common_measure_unit") Long id_common_measure_unit,
            @QueryParam("name_measure_unit") String name_measure_unit,
            @QueryParam("description_measure_unit") String description_measure_unit,
            @QueryParam("id_state_measure_unit") Long id_state_measure_unit,
            @QueryParam("state") Integer state_measure_unit,
            @QueryParam("creation_measure_unit") Date creation_measure_unit,
            @QueryParam("modify_measure_unit") Date modify_measure_unit,

            @QueryParam("id_product") Long id_product,
            @QueryParam("id_attribute_list") Long id_attribute_list,

            @QueryParam("id_category")  Long id_category,
                    @QueryParam("stock") Integer stock,
                    @QueryParam("stock_min") Integer stock_min,
                    @QueryParam("img_url") String img_url,
                    @QueryParam("code_pro") String code_pro,
                    @QueryParam("id_tax") Long id_tax,

                    @QueryParam("id_common_product") Long id_common_product,
                    @QueryParam("name_product") String name_product,
                    @QueryParam("description_product") String description_product,

                    @QueryParam("id_state_product") Long id_state_product,
                    @QueryParam("state_product") Integer state_product,
                    @QueryParam("creation_product") Date creation_product,
                    @QueryParam("modify_product") Date modify_product
    )
    {

        Product product = new Product(id_product, id_category, stock, stock_min, img_url,
                code_pro, id_tax,
                new Common(
                        id_common_product,
                        name_product,
                        description_product
                ),
                new CommonState(id_state_product, state_product,
                        creation_product, modify_product)
        );

        Response response;

        ProductThirdSimple productThirdSimple = new ProductThirdSimple(id_product_third, id_third, standard_price,
                min_price, code,id_category_third,location,
                new CommonState(
                        id_state_prod_third,
                        state_prod_third,
                        creation_prod_third,
                        modify_prod_third
                ),

                new MeasureUnit(
                        id_measure_unit,
                        id_measure_unit_father,
                        new Common(
                                id_common_measure_unit,
                                name_measure_unit,
                                description_measure_unit
                        ),
                        new CommonState(
                                id_state_measure_unit,
                                state_measure_unit,
                                creation_measure_unit,
                                modify_measure_unit
                        )
                ),

                id_product,
                id_attribute_list
        );


        Either<IException, List<ProductThird>> getProducts = productThirdBusiness.getProductThird(new ProductThird(product,productThirdSimple));

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value().size());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePersonThirdResource(@PathParam("id") Long id_product){
        Response response;
        Either<IException, Long> allViewOffertsEither = productThirdBusiness.deleteProductThird(id_product);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateProductThirdResource(@PathParam("id") Long id_product_third, ProductThirdDTO productThirdDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = productThirdBusiness.updateProductThird(id_product_third, productThirdDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }



}
