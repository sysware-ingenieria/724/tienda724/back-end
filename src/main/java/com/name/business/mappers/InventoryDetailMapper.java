package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryDetailMapper implements ResultSetMapper<InventoryDetail> {
    @Override
    public InventoryDetail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InventoryDetail(
                new InventoryDetailSimple(
                        resultSet.getLong("ID_INVENTORY_DETAIL"),
                        resultSet.getLong("ID_INVENTORY"),
                        resultSet.getLong("ID_PRODUCT_THIRD"),
                        resultSet.getInt("QUANTITY"),
                        resultSet.getString("CODE_INV"),
                        new CommonState(
                                resultSet.getLong("ID_STATE_INV"),
                                resultSet.getInt("STATE_INV"),
                                resultSet.getDate("CREATION_INVENTORY_DETAIL"),
                                resultSet.getDate("MODIFY_INVENTORY_DETAIL")
                        )
                ),
                new Product(
                        resultSet.getLong("ID_PRODUCT"),
                        resultSet.getLong("ID_CATEGORY"),
                        resultSet.getInt("STOCK"),
                        resultSet.getInt("STOCK_MIN"),
                        resultSet.getString("IMG_URL"),
                        resultSet.getString("CODE_PRO"),
                        resultSet.getLong("ID_TAX"),
                        new Common(
                                resultSet.getLong("ID_COMMON_PRO"),
                                resultSet.getString("NAME"),
                                resultSet.getString("DESCRIPTION")
                        ),
                        new CommonState(
                                resultSet.getLong("ID_STATE_PRO"),
                                resultSet.getInt("STATE_PRO"),
                                resultSet.getDate("CREATION_PRODUCT"),
                                resultSet.getDate("MODIFY_PRODUCT")
                        )
                ),
                new ProductThirdSimple(
                        resultSet.getLong("ID_PRODUCT_THIRD"),
                        resultSet.getLong("ID_THIRD"),
                        resultSet.getDouble("STANDARD_PRICE"),
                        resultSet.getDouble("MIN_PRICE"),
                        resultSet.getString("CODE"),
                        resultSet.getLong("ID_CATEGORY"),
                        resultSet.getString("LOCATION_PR_TH"),
                        new CommonState(
                                resultSet.getLong("ID_STATE"),
                                resultSet.getInt("STATE"),
                                resultSet.getDate("CREATION_PRODUCT_THIRD"),
                                resultSet.getDate("MODIFY_PRODUCT_THIRD")
                        ),
                        new MeasureUnit(
                                resultSet.getLong("ID_MEASURE_UNIT"),
                                resultSet.getLong("ID_MEASURE_UNIT_FATHER"),
                                new Common(
                                        resultSet.getLong("ID_COMMON_UNT"),
                                        resultSet.getString("NAME_UNT"),
                                        resultSet.getString("DESCRIPTION_UNT")
                                ),
                                new CommonState(
                                        resultSet.getLong("ID_STATE_UNT"),
                                        resultSet.getInt("STATE_UNT"),
                                        resultSet.getDate("CREATION_MEASURE_UNIT"),
                                        resultSet.getDate("MODIFY_MEASURE_UNIT")
                                )
                        ),
                        resultSet.getLong("ID_PRODUCT"),
                        resultSet.getLong("ID_ATTRIBUTE_LIST")

                )

        );
    }
}
