package com.name.business.mappers;

import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.Inventory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryMapper implements ResultSetMapper<Inventory> {

    @Override
    public Inventory map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Inventory(
                resultSet.getLong("ID_INVENTORY"),
                resultSet.getLong("ID_THIRD"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")
                ),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_INVENTORY"),
                        resultSet.getDate("MODIFY_INVENTORY")
                )
        );
    }
}
