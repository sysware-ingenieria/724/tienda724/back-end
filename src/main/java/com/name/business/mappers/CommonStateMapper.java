package com.name.business.mappers;

import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommonStateMapper implements ResultSetMapper<CommonState> {
    @Override
    public CommonState map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CommonState(
                resultSet.getLong("ID_COMMON_STATE"),
                resultSet.getInt("STATE"),
                resultSet.getDate("CREATION_DATE"),
                resultSet.getDate("MODIFY_DATE")
        );
    }
}
