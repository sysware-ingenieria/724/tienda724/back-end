package com.name.business.mappers;


import com.name.business.entities.CommonSimple;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommonSimpleMapper implements ResultSetMapper<CommonSimple> {

    @Override
    public CommonSimple map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CommonSimple(
                resultSet.getLong("ID"),
                resultSet.getLong("ID_COMMON"),
                resultSet.getLong("ID_COMMON_STATE")
        );
    }
}
