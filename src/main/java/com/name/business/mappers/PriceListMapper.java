package com.name.business.mappers;

import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.PriceList;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PriceListMapper  implements ResultSetMapper<PriceList>{
    @Override
    public PriceList map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PriceList(
                resultSet.getLong("ID_PRICE_LIST"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getDouble("STANDARD_PRICE"),
                resultSet.getDouble("MIN_PRICE"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")
                ),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_PRICE_LIST"),
                        resultSet.getDate("MODIFY_PRICE_LIST")
                )

        );
    }
}
