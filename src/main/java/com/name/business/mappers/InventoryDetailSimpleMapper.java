package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.InventoryDetailSimple;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryDetailSimpleMapper implements ResultSetMapper<InventoryDetailSimple> {
    @Override
    public InventoryDetailSimple map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InventoryDetailSimple(
                resultSet.getLong("ID_INVENTORY_DETAIL"),
                resultSet.getLong("ID_INVENTORY"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getInt("QUANTITY"),
                resultSet.getString("CODE"),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_INVENTORY_DETAIL"),
                        resultSet.getDate("MODIFY_INVENTORY_DETAIL")
                )
        );
    }
}
