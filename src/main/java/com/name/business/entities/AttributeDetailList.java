package com.name.business.entities;

import java.util.Date;

public class AttributeDetailList {

    private Long id_attribute_detail_list;
    private Long id_attribute_list;
    private Long id_state_attrib_detail_list;
    private Integer state_attrib_detail_list;
    private Date creation_attrib_detail_list;
    private Date modify_attrib_detail_list;

    private AttributeValue value;

    public AttributeDetailList(Long id_attribute_detail_list, Long id_attribute_list, CommonState state, AttributeValue value) {
        this.id_attribute_detail_list = id_attribute_detail_list;
        this.id_attribute_list = id_attribute_list;
        this.id_state_attrib_detail_list = state.getId_common_state();
        this.state_attrib_detail_list = state.getState();
        this.creation_attrib_detail_list = state.getCreation_date();
        this.modify_attrib_detail_list = state.getModify_date();
        this.value = value;
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_attrib_detail_list(),
                this.getState_attrib_detail_list(),
                this.getCreation_attrib_detail_list(),
                this.getModify_attrib_detail_list()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_attrib_detail_list =state.getId_common_state();
        this.state_attrib_detail_list =state.getState();
        this.creation_attrib_detail_list =state.getCreation_date();
        this.modify_attrib_detail_list =state.getModify_date();
    }

    public AttributeDetailList(Long id_attribute_detail_list, Long id_attribute_list, Long id_state_attrib_detail_list, Integer state_attrib_detail_list, Date creation_attrib_deatail_list, Date modify_attrib_detail_list, AttributeValue value) {
        this.id_attribute_detail_list = id_attribute_detail_list;
        this.id_attribute_list = id_attribute_list;

        this.value = value;
    }

    public Long getId_state_attrib_detail_list() {
        return id_state_attrib_detail_list;
    }

    public void setId_state_attrib_detail_list(Long id_state_attrib_detail_list) {
        this.id_state_attrib_detail_list = id_state_attrib_detail_list;
    }

    public Integer getState_attrib_detail_list() {
        return state_attrib_detail_list;
    }

    public void setState_attrib_detail_list(Integer state_attrib_detail_list) {
        this.state_attrib_detail_list = state_attrib_detail_list;
    }

    public Date getCreation_attrib_detail_list() {
        return creation_attrib_detail_list;
    }

    public void setCreation_attrib_detail_list(Date creation_attrib_detail_list) {
        this.creation_attrib_detail_list = creation_attrib_detail_list;
    }

    public Date getModify_attrib_detail_list() {
        return modify_attrib_detail_list;
    }

    public void setModify_attrib_detail_list(Date modify_attrib_detail_list) {
        this.modify_attrib_detail_list = modify_attrib_detail_list;
    }

    public Long getId_attribute_detail_list() {
        return id_attribute_detail_list;
    }

    public void setId_attribute_detail_list(Long id_attribute_detail_list) {
        this.id_attribute_detail_list = id_attribute_detail_list;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }



    public AttributeValue getValue() {
        return value;
    }

    public void setValue(AttributeValue value) {
        this.value = value;
    }
}
