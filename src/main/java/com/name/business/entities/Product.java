package com.name.business.entities;

import java.util.Date;

public class Product {
    private Long id_product;
    private Long id_category;
    private Integer stock;
    private Integer stock_min;
    private String img_url;
    private String code;
    private Long id_tax;

    private Long id_common_product;
    private String name_product;
    private String description_product;

    private Long id_state_product;
    private Integer state_product;
    private Date creation_product;
    private Date modify_product;



    public Product(Long id_product, Long id_category, Integer stock, Integer stock_min, String img_url, String code,
                   Long id_tax, Common common, CommonState state) {
        this.id_product = id_product;
        this.id_category = id_category;
        this.stock = stock;
        this.stock_min = stock_min;
        this.img_url = img_url;
        this.code = code;
        this.id_tax = id_tax;

        this.id_common_product = common.getId_common();
        this.name_product = common.getName();
        this.description_product = common.getDescription();

        this.id_state_product = state.getId_common_state();
        this.state_product = state.getState();
        this.creation_product = state.getCreation_date();
        this.modify_product = state.getModify_date();
    }


    public Common loadingCommon() {
        return new Common(this.getId_common_product(),
                this.getName_product(),
                this.getDescription_product());
    }

    public void setCommon(Common common) {
        this.id_common_product = common.getId_common();
        this.name_product = common.getName();
        this.description_product = common.getDescription();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_product(),
                this.getState_product(),
                this.getCreation_product(),
                this.getModify_product()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_product=state.getId_common_state();
        this.state_product=state.getState();
        this.creation_product=state.getCreation_date();
        this.modify_product=state.getModify_date();
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_category() {
        return id_category;
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStock_min() {
        return stock_min;
    }

    public void setStock_min(Integer stock_min) {
        this.stock_min = stock_min;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId_tax() {
        return id_tax;
    }

    public void setId_tax(Long id_tax) {
        this.id_tax = id_tax;
    }

    public Long getId_common_product() {
        return id_common_product;
    }

    public void setId_common_product(Long id_common_product) {
        this.id_common_product = id_common_product;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public String getDescription_product() {
        return description_product;
    }

    public void setDescription_product(String description_product) {
        this.description_product = description_product;
    }

    public Long getId_state_product() {
        return id_state_product;
    }

    public void setId_state_product(Long id_state_product) {
        this.id_state_product = id_state_product;
    }

    public Integer getState_product() {
        return state_product;
    }

    public void setState_product(Integer state_product) {
        this.state_product = state_product;
    }

    public Date getCreation_product() {
        return creation_product;
    }

    public void setCreation_product(Date creation_product) {
        this.creation_product = creation_product;
    }

    public Date getModify_product() {
        return modify_product;
    }

    public void setModify_product(Date modify_product) {
        this.modify_product = modify_product;
    }
}
