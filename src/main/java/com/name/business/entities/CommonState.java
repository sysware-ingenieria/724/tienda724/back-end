package com.name.business.entities;

import java.util.Date;

public class CommonState {

    private Long id_common_state;
    private Integer state;
    private Date creation_date;
    private Date modify_date;

    public CommonState(Long id_common_state, Integer state, Date creation_date, Date modify_date) {
        this.id_common_state = id_common_state;
        this.state = state;
        this.creation_date = creation_date;
        this.modify_date = modify_date;
    }

    public Long getId_common_state() {
        return id_common_state;
    }

    public void setId_common_state(Long id_common_state) {
        this.id_common_state = id_common_state;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
