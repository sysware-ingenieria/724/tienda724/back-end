package com.name.business.entities;

import java.util.Date;

public class AttributeValue {
    private Long id_attribute_value;
    private Long id_attribute;
    private Long id_common_attrib_value;
    private String name_attrib_value;
    private String description_attrib_value;
    private Long id_state_attrib_value;
    private Integer state_attrib_value;
    private Date creation_attrib_value;
    private Date modify_attrib_value;


    public AttributeValue(Long id_attribute_value, Long id_attribute, Common common, CommonState state) {
        this.id_attribute_value = id_attribute_value;
        this.id_attribute = id_attribute;
        this.id_common_attrib_value = common.getId_common();
        this.name_attrib_value = common.getName();
        this.description_attrib_value = common.getDescription();
        this.id_state_attrib_value = state.getId_common_state();
        this.state_attrib_value = state.getState();
        this.creation_attrib_value = state.getCreation_date();
        this.modify_attrib_value = state.getModify_date();
    }



    public Common loadingCommon() {
        return new Common(this.getId_common_attrib_value(),
                this.getName_attrib_value(),
                this.getDescription_attrib_value());
    }

    public void setCommon(Common common) {
        this.id_common_attrib_value = common.getId_common();
        this.name_attrib_value = common.getName();
        this.description_attrib_value = common.getDescription();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_attrib_value(),
                this.getState_attrib_value(),
                this.getCreation_attrib_value(),
                this.getModify_attrib_value()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_attrib_value=state.getId_common_state();
        this.state_attrib_value=state.getState();
        this.creation_attrib_value=state.getCreation_date();
        this.modify_attrib_value=state.getModify_date();
    }



    public Long getId_attribute_value() {
        return id_attribute_value;
    }

    public void setId_attribute_value(Long id_attribute_value) {
        this.id_attribute_value = id_attribute_value;
    }

    public Long getId_attribute() {
        return id_attribute;
    }

    public void setId_attribute(Long id_attribute) {
        this.id_attribute = id_attribute;
    }

    public Long getId_common_attrib_value() {
        return id_common_attrib_value;
    }

    public void setId_common_attrib_value(Long id_common_attrib_value) {
        this.id_common_attrib_value = id_common_attrib_value;
    }

    public String getName_attrib_value() {
        return name_attrib_value;
    }

    public void setName_attrib_value(String name_attrib_value) {
        this.name_attrib_value = name_attrib_value;
    }

    public String getDescription_attrib_value() {
        return description_attrib_value;
    }

    public void setDescription_attrib_value(String description_attrib_value) {
        this.description_attrib_value = description_attrib_value;
    }

    public Long getId_state_attrib_value() {
        return id_state_attrib_value;
    }

    public void setId_state_attrib_value(Long id_state_attrib_value) {
        this.id_state_attrib_value = id_state_attrib_value;
    }

    public Integer getState_attrib_value() {
        return state_attrib_value;
    }

    public void setState_attrib_value(Integer state_attrib_value) {
        this.state_attrib_value = state_attrib_value;
    }

    public Date getCreation_attrib_value() {
        return creation_attrib_value;
    }

    public void setCreation_attrib_value(Date creation_attrib_value) {
        this.creation_attrib_value = creation_attrib_value;
    }

    public Date getModify_attrib_value() {
        return modify_attrib_value;
    }

    public void setModify_attrib_value(Date modify_attrib_value) {
        this.modify_attrib_value = modify_attrib_value;
    }
}
