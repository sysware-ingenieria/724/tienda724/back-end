package com.name.business.entities;

import java.util.Date;

public class ProductThirdSimple {

    private Long id_product_third;
    private Long id_third;
    private Long id_product;
    private Long id_attribute_list;
    private Double standard_price;
    private Double min_price;
    private String code;
    private Long id_category_third;
    private String location;

    private Long id_state_prod_third;
    private Integer state_prod_third;
    private Date creation_prod_third;
    private Date modify_prod_third;

    private MeasureUnit measure_unit;



    public ProductThirdSimple(Long id_product_third, Long id_third, Double standard_price, Double min_price, String code,
                              Long id_category_third, String location,
                              CommonState state,
                              MeasureUnit measure_unit, Long id_product, Long id_attribute_list ) {

        this.id_product_third = id_product_third;
        this.id_third = id_third;
        this.standard_price = standard_price;
        this.min_price = min_price;
        this.code = code;
        this.id_category_third = id_category_third;
        this.location=location;

        this.id_product = id_product;



        this.id_attribute_list = id_attribute_list;
        this.id_state_prod_third = state.getId_common_state();
        this.state_prod_third = state.getState();
        this.creation_prod_third = state.getCreation_date();
        this.modify_prod_third = state.getModify_date();
        this.measure_unit=measure_unit;
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_prod_third(),
                this.getState_prod_third(),
                this.getCreation_prod_third(),
                this.getModify_prod_third()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_prod_third = state.getId_common_state();
        this.state_prod_third = state.getState();
        this.creation_prod_third = state.getCreation_date();
        this.modify_prod_third = state.getModify_date();
    }

    public Long getId_category_third() {
        return id_category_third;
    }

    public void setId_category_third(Long id_category_third) {
        this.id_category_third = id_category_third;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }

    public Double getStandard_price() {
        return standard_price;
    }

    public void setStandard_price(Double standard_price) {
        this.standard_price = standard_price;
    }

    public Double getMin_price() {
        return min_price;
    }

    public void setMin_price(Double min_price) {
        this.min_price = min_price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId_state_prod_third() {
        return id_state_prod_third;
    }

    public void setId_state_prod_third(Long id_state_prod_third) {
        this.id_state_prod_third = id_state_prod_third;
    }

    public Integer getState_prod_third() {
        return state_prod_third;
    }

    public void setState_prod_third(Integer state_prod_third) {
        this.state_prod_third = state_prod_third;
    }

    public Date getCreation_prod_third() {
        return creation_prod_third;
    }

    public void setCreation_prod_third(Date creation_prod_third) {
        this.creation_prod_third = creation_prod_third;
    }

    public Date getModify_prod_third() {
        return modify_prod_third;
    }

    public void setModify_prod_third(Date modify_prod_third) {
        this.modify_prod_third = modify_prod_third;
    }

    public MeasureUnit getMeasure_unit() {
        return measure_unit;
    }

    public void setMeasure_unit(MeasureUnit measure_unit) {
        this.measure_unit = measure_unit;
    }
}