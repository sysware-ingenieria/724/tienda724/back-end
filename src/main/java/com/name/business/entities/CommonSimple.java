package com.name.business.entities;

public class CommonSimple {

    private Long id;
    private Long id_common;
    private Long id_common_state;

    public CommonSimple(Long id, Long id_common, Long id_common_state) {
        this.id = id;
        this.id_common = id_common;
        this.id_common_state = id_common_state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Long getId_common_state() {
        return id_common_state;
    }

    public void setId_common_state(Long id_common_state) {
        this.id_common_state = id_common_state;
    }
}
