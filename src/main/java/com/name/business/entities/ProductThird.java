package com.name.business.entities;

import java.util.Date;

public class ProductThird {

    private Product product;
    private ProductThirdSimple  description;

    public ProductThird(Product product, ProductThirdSimple description) {
        this.product = product;
        this.description = description;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductThirdSimple getDescription() {
        return description;
    }

    public void setDescription(ProductThirdSimple description) {
        this.description = description;
    }
}