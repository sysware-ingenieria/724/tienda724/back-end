package com.name.business.entities;

import java.util.Date;

public class Inventory {

    private Long id_inventory;
    private Long id_third;

    private Long id_common_inventory;
    private String name_inventory;
    private String description_inventory;
    private Long id_state_inventory;
    private Integer state_inventory;
    private Date creation_inventory;
    private Date modify_inventory;


    public Inventory(Long id_inventory, Long id_third, Common common, CommonState state) {
        this.id_inventory = id_inventory;
        this.id_third = id_third;
        this.id_common_inventory = common.getId_common();
        this.name_inventory = common.getName();
        this.description_inventory = common.getDescription();
        this.id_state_inventory = state.getId_common_state();
        this.state_inventory = state.getState();
        this.creation_inventory = state.getCreation_date();
        this.modify_inventory = state.getModify_date();
    }
    public Common loadingCommon() {
        return new Common(this.getId_common_inventory(),
                this.getName_inventory(),
                this.getDescription_inventory());
    }

    public void setCommon(Common common) {
        this.id_common_inventory = common.getId_common();
        this.name_inventory = common.getName();
        this.description_inventory = common.getDescription();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_inventory(),
                this.getState_inventory(),
                this.getCreation_inventory(),
                this.getModify_inventory()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_inventory=state.getId_common_state();
        this.state_inventory=state.getState();
        this.creation_inventory=state.getCreation_date();
        this.modify_inventory=state.getModify_date();
    }

    public Long getId_inventory() {
        return id_inventory;
    }

    public void setId_inventory(Long id_inventory) {
        this.id_inventory = id_inventory;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_common_inventory() {
        return id_common_inventory;
    }

    public void setId_common_inventory(Long id_common_inventory) {
        this.id_common_inventory = id_common_inventory;
    }

    public String getName_inventory() {
        return name_inventory;
    }

    public void setName_inventory(String name_inventory) {
        this.name_inventory = name_inventory;
    }

    public String getDescription_inventory() {
        return description_inventory;
    }

    public void setDescription_inventory(String description_inventory) {
        this.description_inventory = description_inventory;
    }

    public Long getId_state_inventory() {
        return id_state_inventory;
    }

    public void setId_state_inventory(Long id_state_inventory) {
        this.id_state_inventory = id_state_inventory;
    }

    public Integer getState_inventory() {
        return state_inventory;
    }

    public void setState_inventory(Integer state_inventory) {
        this.state_inventory = state_inventory;
    }

    public Date getCreation_inventory() {
        return creation_inventory;
    }

    public void setCreation_inventory(Date creation_inventory) {
        this.creation_inventory = creation_inventory;
    }

    public Date getModify_inventory() {
        return modify_inventory;
    }

    public void setModify_inventory(Date modify_inventory) {
        this.modify_inventory = modify_inventory;
    }
}
