package com.name.business.entities;

import java.util.Date;

public class MeasureUnit {

    private Long id_measure_unit;
    private Long id_measure_unit_father;

    private Long id_common_measure_unit;
    private String name_measure_unit;
    private String description_measure_unit;

    private Long id_state_measure_unit;
    private Integer state_measure_unit;
    private Date creation_measure_unit;
    private Date modify_measure_unit;

    public MeasureUnit(Long id_measure_unit, Long id_measure_unit_father, Common common, CommonState state) {
        this.id_measure_unit = id_measure_unit;
        this.id_measure_unit_father = id_measure_unit_father;
        this.id_common_measure_unit = common.getId_common();
        this.name_measure_unit = common.getName();
        this.description_measure_unit = common.getDescription();
        this.id_state_measure_unit = state.getId_common_state();
        this.state_measure_unit = state.getState();
        this.creation_measure_unit = state.getCreation_date();
        this.modify_measure_unit = state.getModify_date();
    }


    public Common loadingCommon() {
        return new Common(
                this.getId_common_measure_unit(),
                this.getName_measure_unit(),
                this.getDescription_measure_unit());
    }

    public void setCommon(Common common) {
        this.id_common_measure_unit = common.getId_common();
        this.name_measure_unit = common.getName();
        this.description_measure_unit = common.getDescription();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_measure_unit(),
                this.getState_measure_unit(),
                this.getCreation_measure_unit(),
                this.getModify_measure_unit()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_measure_unit =state.getId_common_state();
        this.state_measure_unit=state.getState();
        this.creation_measure_unit =state.getCreation_date();
        this.modify_measure_unit =state.getModify_date();
    }






    public Long getId_measure_unit() {
        return id_measure_unit;
    }

    public void setId_measure_unit(Long id_measure_unit) {
        this.id_measure_unit = id_measure_unit;
    }

    public Long getId_measure_unit_father() {
        return id_measure_unit_father;
    }

    public void setId_measure_unit_father(Long id_measure_unit_father) {
        this.id_measure_unit_father = id_measure_unit_father;
    }

    public Long getId_common_measure_unit() {
        return id_common_measure_unit;
    }

    public void setId_common_measure_unit(Long id_common_measure_unit) {
        this.id_common_measure_unit = id_common_measure_unit;
    }

    public String getName_measure_unit() {
        return name_measure_unit;
    }

    public void setName_measure_unit(String name_measure_unit) {
        this.name_measure_unit = name_measure_unit;
    }

    public String getDescription_measure_unit() {
        return description_measure_unit;
    }

    public void setDescription_measure_unit(String description_measure_unit) {
        this.description_measure_unit = description_measure_unit;
    }

    public Long getId_state_measure_unit() {
        return id_state_measure_unit;
    }

    public void setId_state_measure_unit(Long id_state_measure_unit) {
        this.id_state_measure_unit = id_state_measure_unit;
    }

    public Integer getState_measure_unit() {
        return state_measure_unit;
    }

    public void setState_measure_unit(Integer state_measure_unit) {
        this.state_measure_unit = state_measure_unit;
    }

    public Date getCreation_measure_unit() {
        return creation_measure_unit;
    }

    public void setCreation_measure_unit(Date creation_measure_unit) {
        this.creation_measure_unit = creation_measure_unit;
    }

    public Date getModify_measure_unit() {
        return modify_measure_unit;
    }

    public void setModify_measure_unit(Date modify_measure_unit) {
        this.modify_measure_unit = modify_measure_unit;
    }
}
