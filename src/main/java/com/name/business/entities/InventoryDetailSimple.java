package com.name.business.entities;

import java.util.Date;

public class InventoryDetailSimple {
    private Long id_inventory_detail;
    private Long id_inventory;
    private Long id_product_third;
    private Integer quantity;
    private String code;


    private Long id_state_inv_detail;
    private Integer state_inv_detail;
    private Date creation_inv_detail;
    private Date modify_inv_detail;



    public InventoryDetailSimple(Long id_inventory_detail, Long id_inventory, Long id_product_third, Integer quantity, String code,
                                 CommonState state) {
        this.id_inventory_detail = id_inventory_detail;
        this.id_inventory = id_inventory;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.code = code;
        this.id_state_inv_detail = state.getId_common_state();
        this.state_inv_detail = state.getState();
        this.creation_inv_detail = state.getCreation_date();
        this.modify_inv_detail = state.getModify_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_inv_detail(),
                this.getState_inv_detail(),
                this.getCreation_inv_detail(),
                this.getModify_inv_detail()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_inv_detail=state.getId_common_state();
        this.state_inv_detail=state.getState();
        this.creation_inv_detail=state.getCreation_date();
        this.modify_inv_detail=state.getModify_date();
    }


    public Long getId_inventory_detail() {
        return id_inventory_detail;
    }

    public void setId_inventory_detail(Long id_inventory_detail) {
        this.id_inventory_detail = id_inventory_detail;
    }

    public Long getId_inventory() {
        return id_inventory;
    }

    public void setId_inventory(Long id_inventory) {
        this.id_inventory = id_inventory;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId_state_inv_detail() {
        return id_state_inv_detail;
    }

    public void setId_state_inv_detail(Long id_state_inv_detail) {
        this.id_state_inv_detail = id_state_inv_detail;
    }

    public Integer getState_inv_detail() {
        return state_inv_detail;
    }

    public void setState_inv_detail(Integer state_inv_detail) {
        this.state_inv_detail = state_inv_detail;
    }

    public Date getCreation_inv_detail() {
        return creation_inv_detail;
    }

    public void setCreation_inv_detail(Date creation_inv_detail) {
        this.creation_inv_detail = creation_inv_detail;
    }

    public Date getModify_inv_detail() {
        return modify_inv_detail;
    }

    public void setModify_inv_detail(Date modify_inv_detail) {
        this.modify_inv_detail = modify_inv_detail;
    }
}
