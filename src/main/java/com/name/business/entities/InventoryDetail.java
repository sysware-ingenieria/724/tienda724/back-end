package com.name.business.entities;

public class InventoryDetail {
    private InventoryDetailSimple detail;
    private Product product;
    private ProductThirdSimple description;

    public InventoryDetail(InventoryDetailSimple detail, Product product, ProductThirdSimple description) {
        this.detail = detail;
        this.product = product;
        this.description = description;
    }

    public InventoryDetailSimple getDetail() {
        return detail;
    }

    public void setDetail(InventoryDetailSimple detail) {
        this.detail = detail;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductThirdSimple getDescription() {
        return description;
    }

    public void setDescription(ProductThirdSimple description) {
        this.description = description;
    }
}
