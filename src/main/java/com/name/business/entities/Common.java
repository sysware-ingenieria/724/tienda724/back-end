package com.name.business.entities;

public class Common {

    private Long id_common;
    private String name;
    private String description;

    public Common(Long id_common, String name, String description) {
        this.id_common = id_common;
        this.name = name;
        this.description = description;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
