package com.name.business.DAOs;

import com.name.business.entities.CommonState;
import com.name.business.mappers.CommonStateMapper;
import com.name.business.representations.CommonStateDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(CommonStateMapper.class)
public interface CommonStateDAO {

    @SqlQuery("SELECT ID_COMMON_STATE FROM COMMON_STATE WHERE ID_COMMON_STATE IN (SELECT MAX(COMMON_STATE.ID_COMMON_STATE) FROM COMMON_STATE)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO COMMON_STATE ( STATE,CREATION_DATE,MODIFY_DATE) VALUES (:co_st.state,:co_st.creation_date,:co_st.modify_date)")
    void create(@BindBean("co_st")CommonStateDTO commonStateDTO);


    /**
     *
     * @param commonState
     * @return
     */
    @SqlQuery("SELECT * FROM COMMON_STATE " +
            " WHERE ( ID_COMMON_STATE = :co_st.id_common_state OR :co_st.id_common_state IS NULL) AND " +
            "       ( STATE =:co_st.state OR :co_st.state IS NULL ) AND " +
            "       ( CREATION_DATE = :co_st.creation_date OR :co_st.creation_date IS NULL ) AND " +
            "       ( MODIFY_DATE = :co_st.modify_date OR :co_st.modify_date IS NULL )")
    List<CommonState> read(@BindBean("co_st") CommonState commonState);

    /**
     *
     * @param id_common_state
     * @param commonStateDTO
     * @return
     */
    @SqlUpdate("UPDATE COMMON_STATE SET " +
            "    STATE =:co_st.state," +
            "    CREATION_DATE = :co_st.creation_date ," +
            "    MODIFY_DATE = :co_st.modify_date" +
            "  WHERE  (ID_COMMON_STATE = :id_common_state)")
    int update(@Bind("id_common_state") Long id_common_state, @BindBean("co_st") CommonStateDTO commonStateDTO);

    /**
     * Delete: Change State
     * @param id_common_state
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON_STATE SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON_STATE = :id_common_state)")
    int delete(@Bind("id_common_state") Long id_common_state, @Bind("modify_date") Date date);

    /**
     * Delete: Permanent!
     * @param commonState
     * @return
     */
    @SqlUpdate("DELETE FROM COMMON_STATE" +
            "WHERE ( STATE = :co_st.state OR :co_st.state IS NULL ) AND " +
            "      ( CREATION_DATE = :co_st.creation_date OR :co_st.creation_date IS NULL )AND " +
            "      ( MODIFY_DATE = :co_st.modify_date OR :co_st.modify_date IS NULL ) AND " +
            "      ( ID_COMMON_STATE = :co_st.id_common_state OR :co_st.id_common_state IS NULL )")
    int permanentDelete( @BindBean("co_st") CommonState commonState);




}
