package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.Inventory;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.InventoryMapper;
import com.name.business.representations.InventoryDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
@RegisterMapper(InventoryMapper.class)
public interface InventoryDAO {

    @SqlQuery("SELECT ID_INVENTORY FROM INVENTORY WHERE ID_INVENTORY IN (SELECT MAX(ID_INVENTORY) FROM INVENTORY)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO INVENTORY ( ID_THIRD,ID_COMMON,ID_COMMON_STATE) VALUES (:id_third,:id_common,:id_common_state)")
    void create( @Bind("id_third")Long aLong, @Bind("id_common") Long id_common, @Bind("id_common_state") Long id_common_state);

    @SqlQuery("SELECT COUNT(ID_INVENTORY) FROM INVENTORY WHERE ID_INVENTORY = :id_inventory")
    Integer getValidatorID(@Bind("id_inventory") Long id_inventory);

    @SqlQuery("SELECT * FROM V_INVENTORY V_INV " +
            "WHERE (V_INV.ID_INVENTORY=:inv.id_inventory OR :inv.id_inventory IS NULL ) AND " +
            "      (V_INV.ID_THIRD=:inv.id_third OR :inv.id_third IS NULL ) AND " +
            "      (V_INV.ID_COMMON=:inv.id_common_inventory  OR :inv.id_common_inventory IS NULL ) AND " +
            "      (V_INV.NAME LIKE :inv.name_inventory  OR :inv.name_inventory IS NULL ) AND " +
            "      (V_INV.DESCRIPTION LIKE :inv.description_inventory  OR :inv.description_inventory IS NULL ) AND " +
            "      (V_INV.ID_STATE=:inv.id_state_inventory  OR :inv.id_state_inventory IS NULL ) AND " +
            "      (V_INV.STATE=:inv.state_inventory  OR :inv.state_inventory IS NULL ) AND " +
            "      (V_INV.CREATION_INVENTORY=:inv.creation_inventory  OR :inv.creation_inventory IS NULL ) AND " +
            "      (V_INV.MODIFY_INVENTORY=:inv.modify_inventory  OR :inv.modify_inventory IS NULL ) ")
    List<Inventory> read(@BindBean("inv") Inventory inventory);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_INVENTORY ID, ID_COMMON, ID_COMMON_STATE FROM INVENTORY " +
            "  WHERE (ID_INVENTORY = :id_inventory OR :id_inventory IS NULL) ")
    List<CommonSimple> readCommons(@Bind("id_inventory") Long id_inventory);

    @SqlUpdate("UPDATE INVENTORY SET  " +
            "   ID_THIRD = :id_third " +
            "    WHERE " +
            "            (ID_INVENTORY =  :id_inventory)")
    void update(@Bind("id_inventory") Long id_inventory,@Bind("id_third") Long id_third);
}
