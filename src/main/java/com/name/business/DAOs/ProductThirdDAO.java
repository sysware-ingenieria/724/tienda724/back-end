package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.ProductThirdMapper;
import com.name.business.mappers.ProductThirdSimpleMapper;
import com.name.business.representations.ProductThirdDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;


import java.util.List;

@RegisterMapper(ProductThirdSimpleMapper.class)
public interface ProductThirdDAO {


    @SqlQuery("SELECT ID_PRODUCT_THIRD FROM PRODUCT_THIRD WHERE ID_PRODUCT_THIRD IN (SELECT MAX(ID_PRODUCT_THIRD) FROM PRODUCT_THIRD)")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_PRODUCT_THIRD) FROM PRODUCT_THIRD WHERE ID_PRODUCT_THIRD = :id_product_third")
    Integer getValidatorID(@Bind("id_product_third") Long id_product_third);

    @SqlUpdate("INSERT INTO PRODUCT_THIRD (ID_THIRD,STANDARD_PRICE,MIN_PRICE,ID_PRODUCT,ID_MEASURE_UNIT,ID_COMMON_STATE,ID_ATTRIBUTE_LIST,CODE,ID_CATEGORY,LOCATION) " +
            " VALUES (:pro_th.id_third,:pro_th.standard_price,:pro_th.min_price,:pro_th.id_product, " +
            "         :pro_th.id_measure_unit,:id_common_state, :pro_th.id_attribute_list,:pro_th.code,:pro_th.id_category_third,:pro_th.location)")
    void create(@BindBean("pro_th") ProductThirdDTO productThirdDTO,@Bind("id_common_state") Long id_common_state);

    @SqlQuery("SELECT * FROM V_PRODUCT_THIRD V_PROD_TH WHERE \n" +
            "  ( V_PROD_TH.ID_PRODUCT_THIRD= :prod_th.id_product_third or :prod_th.id_product_third is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_THIRD= :prod_th.id_third or :prod_th.id_third is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_PRODUCT= :prod_th.id_product or :prod_th.id_product is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_ATTRIBUTE_LIST= :prod_th.id_attribute_list or :prod_th.id_attribute_list is NULL ) AND\n" +
            "  ( V_PROD_TH.STANDARD_PRICE= :prod_th.standard_price or :prod_th.standard_price is NULL ) AND\n" +
            "  ( V_PROD_TH.MIN_PRICE= :prod_th.min_price or :prod_th.min_price is NULL ) AND\n" +
            "  ( V_PROD_TH.CODE= :prod_th.code or :prod_th.code is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_STATE= :prod_th.id_state_prod_third or :prod_th.id_state_prod_third is NULL ) AND\n" +
            "  ( V_PROD_TH.STATE= :prod_th.state_prod_third or :prod_th.state_prod_third is NULL ) AND\n" +
            "  ( V_PROD_TH.CREATION_PRODUCT_THIRD= :prod_th.modify_prod_third or :prod_th.creation_prod_third  is NULL ) AND\n" +
            "  ( V_PROD_TH.MODIFY_PRODUCT_THIRD= :prod_th.modify_prod_third or :prod_th.modify_prod_third is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_MEASURE_UNIT= :measure_unit.id_measure_unit or :measure_unit.id_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_MEASURE_UNIT_FATHER = :measure_unit.id_measure_unit_father or :measure_unit.id_measure_unit_father is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_COMMON_UNT= :measure_unit.id_common_measure_unit or :measure_unit.id_common_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.NAME_UNT LIKE :measure_unit.name_measure_unit or :measure_unit.name_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.DESCRIPTION_UNT LIKE :measure_unit.description_measure_unit or :measure_unit.description_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.ID_STATE_UNT= :measure_unit.id_state_measure_unit or :measure_unit.id_state_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.STATE_UNT= :measure_unit.state_measure_unit or :measure_unit.state_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.CREATION_MEASURE_UNIT= :measure_unit.creation_measure_unit or :measure_unit.creation_measure_unit is NULL ) AND\n" +
            "  ( V_PROD_TH.MODIFY_MEASURE_UNIT= :measure_unit.modify_measure_unit or :measure_unit.modify_measure_unit is NULL )")
    List<ProductThirdSimple> readSimple(@BindBean("prod_th") ProductThirdSimple productThirdSimple, @BindBean("measure_unit")MeasureUnit measureUnit);

    @RegisterMapper(ProductThirdMapper.class)
    @SqlQuery("SELECT * FROM V_PRODUCT_THIRD_COMPLETE V_PRO_TH_CO\n" +
            "WHERE \n" +
            "    (ID_PRODUCT_THIRD=:pro_th.id_product_third OR :pro_th.id_product_third IS NULL) AND\n" +
            "    (ID_THIRD=:pro_th.id_third OR :pro_th.id_third IS NULL) AND\n" +
            "    (ID_PRODUCT=:pro_th.id_product OR :pro_th.id_product IS NULL) AND\n" +
            "    (ID_ATTRIBUTE_LIST=:pro_th.id_attribute_list OR :pro_th.id_attribute_list IS NULL) AND\n" +
            "    (STANDARD_PRICE=:pro_th.standard_price OR :pro_th.standard_price IS NULL) AND\n" +
            "    (MIN_PRICE=:pro_th.min_price OR :pro_th.min_price IS NULL) AND\n" +
            "    (CODE=:pro_th.code OR :pro_th.code IS NULL) AND\n" +
            "    (ID_STATE=:pro_th.id_state_prod_third OR :pro_th.id_state_prod_third IS NULL) AND\n" +
            "    (STATE=:pro_th.state_prod_third OR :pro_th.state_prod_third IS NULL) AND\n" +
            "    (CREATION_PRODUCT_THIRD=:pro_th.creation_prod_third OR :pro_th.creation_prod_third IS NULL) AND\n" +
            "    (MODIFY_PRODUCT_THIRD=:pro_th.modify_prod_third OR :pro_th.modify_prod_third IS NULL) AND\n" +
            " \n" +
            "    (ID_MEASURE_UNIT=:measure_unit.id_measure_unit OR :measure_unit.id_measure_unit IS NULL) AND\n" +
            "    (ID_MEASURE_UNIT_FATHER=:measure_unit.id_measure_unit_father OR :measure_unit.id_measure_unit_father IS NULL) AND\n" +
            "    (ID_COMMON_UNT=:measure_unit.id_common_measure_unit OR :measure_unit.id_common_measure_unit IS NULL) AND\n" +
            "    (NAME_UNT LIKE :measure_unit.name_measure_unit OR :measure_unit.name_measure_unit IS NULL) AND\n" +
            "    (DESCRIPTION_UNT LIKE :measure_unit.description_measure_unit OR :measure_unit.description_measure_unit IS NULL) AND\n" +
            "    (ID_STATE_UNT =:measure_unit.id_state_measure_unit OR :measure_unit.id_state_measure_unit IS NULL) AND\n" +
            "    (STATE_UNT=:measure_unit.state_measure_unit OR :measure_unit.state_measure_unit IS NULL) AND\n" +
            "    (CREATION_MEASURE_UNIT=:measure_unit.creation_measure_unit OR :measure_unit.creation_measure_unit IS NULL) AND\n" +
            "    (MODIFY_MEASURE_UNIT=:measure_unit.modify_measure_unit OR :measure_unit.modify_measure_unit IS NULL) AND\n" +
            "\n" +
            "    (ID_CATEGORY=:pro.id_category OR :pro.id_category IS NULL) AND\n" +
            "    (STOCK=:pro.stock OR :pro.stock IS NULL) AND\n" +
            "    (STOCK_MIN=:pro.stock_min OR :pro.stock_min IS NULL) AND\n" +
            "    (IMG_URL=:pro.img_url OR :pro.img_url IS NULL) AND\n" +
            "    (CODE_PRO=:pro.code OR :pro.code IS NULL) AND\n" +
            "    (ID_TAX=:pro.id_tax OR :pro.id_tax IS NULL) AND\n" +
            "    (ID_COMMON_PRO=:pro.id_common_product OR :pro.id_common_product IS NULL) AND\n" +
            "    (NAME LIKE :pro.name_product OR :pro.name_product IS NULL) AND\n" +
            "    (DESCRIPTION LIKE :pro.description_product OR :pro.description_product IS NULL) AND\n" +
            "    (ID_STATE_PRO=:pro.id_state_product OR :pro.id_state_product IS NULL) AND\n" +
            "    (STATE_PRO=:pro.state_product OR :pro.state_product IS NULL) AND\n" +
            "    (CREATION_PRODUCT=:pro.creation_product OR :pro.creation_product IS NULL) AND\n" +
            "    (MODIFY_PRODUCT=:pro.modify_product OR :pro.modify_product IS NULL)")
    List<ProductThird> read(@BindBean("pro_th") ProductThirdSimple productThirdSimple, @BindBean("measure_unit")MeasureUnit measureUnit, @BindBean("pro")Product product);



    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_PRODUCT_THIRD ID,ID_PRODUCT ID_COMMON, ID_COMMON_STATE FROM PRODUCT_THIRD " +
            "  WHERE (ID_PRODUCT_THIRD = :id_product_third OR :id_product_third IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_product_third") Long id_product_third);

    @SqlUpdate("UPDATE PRODUCT_THIRD SET " +
            "    ID_THIRD = :pro_th.id_third, " +
            "    STANDARD_PRICE = :pro_th.standard_price, " +
            "    MIN_PRICE = :pro_th.min_price, " +
            "    ID_PRODUCT = :pro_th.id_product, " +
            "    ID_MEASURE_UNIT = :pro_th.id_measure_unit," +
            "    ID_ATTRIBUTE_LIST = :pro_th.id_attribute_list," +
            "    CODE = :pro_th.code, " +
            "    ID_CATEGORY = :pro_th.id_category_third, " +
            "    LOCATION = :pro_th.location " +
            "    WHERE  (ID_PRODUCT_THIRD =  :id_product_third)")
    void update( @Bind("id_product_third") Long id_product_third, @BindBean("pro_th") ProductThirdDTO id_inventory);

}
