package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.InventoryDetailMapper;
import com.name.business.mappers.InventoryDetailSimpleMapper;
import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(InventoryDetailSimpleMapper.class)
public interface InventoryDetailDAO {

    @SqlQuery("SELECT ID_INVENTORY_DETAIL FROM INVENTORY_DETAIL WHERE ID_INVENTORY_DETAIL IN (SELECT MAX(ID_INVENTORY_DETAIL) FROM INVENTORY_DETAIL)")
    Long getPkLast();


    @SqlUpdate("\n" +
            "INSERT INTO INVENTORY_DETAIL ( ID_INVENTORY,ID_PRODUCT_THIRD,ID_COMMON_STATE,QUANTITY,CODE) " +
            "VALUES (:id_inventory,:inv_det.id_product_third,:id_common_state,:inv_det.quantity,:inv_det.code) ")
    void create(@Bind("id_inventory") Long id_inventory,@Bind("id_common_state") Long id_common_state,@BindBean("inv_det") InventoryDetailDTO detailDTO);

    @SqlQuery("SELECT * FROM V_INVENTORY_DETAIL V_INV_DET " +
            "WHERE " +
            "    (V_INV_DET.ID_INVENTORY_DETAIL =:inv_det.id_inventory_detail OR :inv_det.id_inventory_detail IS NULL ) AND " +
            "    (V_INV_DET.ID_INVENTORY =:inv_det.id_inventory OR :inv_det.id_inventory IS NULL ) AND " +
            "    (V_INV_DET.ID_PRODUCT_THIRD =:inv_det.id_product_third OR :inv_det.id_product_third IS NULL ) AND" +
            "    (V_INV_DET.QUANTITY =:inv_det.quantity OR :inv_det.quantity IS NULL ) AND" +
            "    (V_INV_DET.CODE =:inv_det.code OR :inv_det.code IS NULL ) AND" +
            "    (V_INV_DET.ID_STATE =:inv_det.id_state_inv_detail OR :inv_det.id_state_inv_detail IS NULL ) AND" +
            "    (V_INV_DET.STATE =:inv_det.state_inv_detail OR :inv_det.state_inv_detail IS NULL ) AND" +
            "    (V_INV_DET.CREATION_INVENTORY_DETAIL =:inv_det.creation_inv_detail OR :inv_det.creation_inv_detail IS NULL ) AND" +
            "    (V_INV_DET.MODIFY_INVENTORY_DETAIL =:inv_det.modify_inv_detail OR :inv_det.modify_inv_detail IS NULL )")
    List<InventoryDetailSimple> readSimple(@BindBean("inv_det")InventoryDetailSimple inventoryDetailSimple);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_INVENTORY_DETAIL ID,ID_INVENTORY ID_COMMON, ID_COMMON_STATE FROM INVENTORY_DETAIL\n" +
            "  WHERE (ID_INVENTORY_DETAIL = :id_inventory_detail OR :id_inventory_detail IS NULL) AND " +
            "        (ID_INVENTORY = :id_inventory OR :id_inventory IS NULL) ")
    List<CommonSimple> readCommons(@Bind("id_inventory_detail") Long id_inventory_detail,@Bind("id_inventory") Long id_inventory);




    @SqlUpdate("UPDATE INVENTORY_DETAIL SET " +
            "   ID_PRODUCT_THIRD = :inv_det.id_product_third,QUANTITY = :inv_det.quantity, CODE=:inv_det.code " +
            "    WHERE (ID_INVENTORY_DETAIL =  :id_inventory_detail OR :id_inventory_detail IS NULL ) AND " +
            "          (ID_INVENTORY =  :id_inventory OR :id_inventory IS NULL )")
    void update( @Bind("id_inventory_detail") Long id_inventory_detail, @Bind("id_inventory") Long id_inventory,
                 @BindBean("inv_det") InventoryDetailDTO detailDTO);

    @RegisterMapper(InventoryDetailMapper.class)
    @SqlQuery("SELECT * FROM V_INVENTORY_DET_COMPLETE V_INV_DET_COM " +
            "WHERE " +
            "    (V_INV_DET_COM.ID_PRODUCT_THIRD=:prod_th.id_product_third OR :prod_th.id_product_third IS NULL )AND " +
            "    (V_INV_DET_COM.ID_THIRD=:prod_th.id_third OR  :prod_th.id_third IS NULL )AND " +
            "    (V_INV_DET_COM.ID_PRODUCT=:prod_th.id_product OR :prod_th.id_product IS NULL )AND " +
            "    (V_INV_DET_COM.ID_ATTRIBUTE_LIST=:prod_th.id_attribute_list OR :prod_th.id_attribute_list IS NULL )AND " +
            "    (V_INV_DET_COM.STANDARD_PRICE=:prod_th.standard_price OR :prod_th.standard_price IS NULL )AND " +
            "    (V_INV_DET_COM.MIN_PRICE=:prod_th.min_price OR :prod_th.min_price IS NULL )AND " +
            "    (V_INV_DET_COM.CODE=:prod_th.code OR :prod_th.code IS NULL )AND " +
            "    (V_INV_DET_COM.ID_STATE=:prod_th.id_state_prod_third OR :prod_th.id_state_prod_third IS NULL )AND " +
            "    (V_INV_DET_COM.STATE=:prod_th.state_prod_third OR :prod_th.state_prod_third IS NULL )AND " +
            "    (V_INV_DET_COM.CREATION_PRODUCT_THIRD=:prod_th.creation_prod_third OR :prod_th.creation_prod_third IS NULL )AND " +
            "    (V_INV_DET_COM.MODIFY_PRODUCT_THIRD=:prod_th.modify_prod_third OR :prod_th.modify_prod_third IS NULL )AND " +
            "    (V_INV_DET_COM.ID_MEASURE_UNIT=:measure_unit.id_measure_unit OR :measure_unit.id_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.ID_MEASURE_UNIT_FATHER=:measure_unit.id_measure_unit_father OR :measure_unit.id_measure_unit_father IS NULL )AND " +
            "    (V_INV_DET_COM.ID_COMMON_UNT=:measure_unit.id_common_measure_unit OR :measure_unit.id_common_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.NAME_UNT LIKE :measure_unit.name_measure_unit OR :measure_unit.name_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.DESCRIPTION_UNT LIKE :measure_unit.description_measure_unit OR :measure_unit.description_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.ID_STATE_UNT=:measure_unit.id_state_measure_unit OR :measure_unit.id_state_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.STATE_UNT=:measure_unit.state_measure_unit OR :measure_unit.state_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.CREATION_MEASURE_UNIT=:measure_unit.creation_measure_unit OR :measure_unit.creation_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.MODIFY_MEASURE_UNIT=:measure_unit.modify_measure_unit OR :measure_unit.modify_measure_unit IS NULL )AND " +
            "    (V_INV_DET_COM.ID_CATEGORY=:pro.id_category OR :pro.id_category IS NULL )AND " +
            "    (V_INV_DET_COM.STOCK=:pro.stock OR :pro.stock IS  NULL )AND " +
            "    (V_INV_DET_COM.STOCK_MIN=:pro.stock_min OR :pro.stock_min IS NULL )AND " +
            "    (V_INV_DET_COM.IMG_URL=:pro.img_url OR :pro.img_url IS NULL )AND " +
            "    (V_INV_DET_COM.CODE_PRO=:pro.code OR :pro.code IS NULL )AND " +
            "    (V_INV_DET_COM.ID_TAX=:pro.id_tax OR :pro.id_tax IS NULL )AND " +
            "    (V_INV_DET_COM.ID_COMMON_PRO=:pro.id_common_product OR :pro.id_common_product IS NULL )AND " +
            "    (V_INV_DET_COM.NAME LIKE :pro.name_product OR :pro.name_product IS NULL )AND " +
            "    (V_INV_DET_COM.DESCRIPTION LIKE :pro.description_product OR :pro.description_product IS NULL )AND " +
            "    (V_INV_DET_COM.ID_STATE_PRO=:pro.id_state_product OR  :pro.id_state_product IS NULL )AND " +
            "    (V_INV_DET_COM.STATE_PRO=:pro.state_product OR :pro.state_product IS NULL )AND " +
            "    (V_INV_DET_COM.CREATION_PRODUCT=:pro.creation_product OR :pro.creation_product IS NULL )AND " +
            "    (V_INV_DET_COM.MODIFY_PRODUCT=:pro.modify_product OR :pro.modify_product IS NULL )AND " +
            " " +
            "    (V_INV_DET_COM.ID_INVENTORY_DETAIL=:inv_det.id_inventory_detail OR :inv_det.id_inventory_detail IS NULL )AND " +
            "    (V_INV_DET_COM.ID_INVENTORY=:inv_det.id_inventory OR :inv_det.id_inventory IS NULL )AND " +
            "    (V_INV_DET_COM.QUANTITY=:inv_det.quantity OR :inv_det.quantity IS NULL )AND " +
            "    (V_INV_DET_COM.CODE_INV=:inv_det.code OR :inv_det.code IS NULL )AND " +
            "    (V_INV_DET_COM.ID_STATE_INV=:inv_det.id_state_inv_detail OR :inv_det.id_state_inv_detail IS NULL )AND " +
            "    (V_INV_DET_COM.STATE_INV=:inv_det.state_inv_detail OR :inv_det.state_inv_detail IS NULL )AND " +
            "    (V_INV_DET_COM.CREATION_INVENTORY_DETAIL=:inv_det.creation_inv_detail OR :inv_det.creation_inv_detail IS NULL )AND " +
            "    (V_INV_DET_COM.MODIFY_INVENTORY_DETAIL=:inv_det.modify_inv_detail OR :inv_det.modify_inv_detail IS NULL )")
    List<InventoryDetail> read(@BindBean("prod_th")ProductThirdSimple productThirdSimple, @BindBean("measure_unit") MeasureUnit measureUnit,
                               @BindBean("pro")Product product, @BindBean("inv_det")InventoryDetailSimple inventoryDetailSimpleSanitation);

    @SqlQuery("SELECT COUNT(ID_INVENTORY) FROM INVENTORY WHERE ID_INVENTORY = :id_inventory")
    Integer getValidatorIDInventory(@Bind("id_inventory") Long id_inventory);

}
