package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.AttributeDetailListMapper;
import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;


@UseStringTemplate3StatementLocator
@RegisterMapper(AttributeDetailListMapper.class)
public interface AttributeDetailListDAO {

    @SqlQuery("SELECT * FROM V_ATTRIBUTE_DETAIL_LIST V_ATT_DET_L " +
            "  WHERE " +
            "    (V_ATT_DET_L.ID_ATTRIBUTE_DETAIL_LIST=:att_det_l.id_attribute_detail_list OR :att_det_l.id_attribute_detail_list IS NULL ) AND " +
            "    (V_ATT_DET_L.ID_ATTRIBUTE_LIST=:att_det_l.id_attribute_list OR :att_det_l.id_attribute_list IS  NULL ) AND " +
            "    (V_ATT_DET_L.ID_STATE_ATT_L_DET=:co_st_adl.id_common_state OR :co_st_adl.id_common_state IS NULL ) AND " +
            "    (V_ATT_DET_L.STATE_ATT_L_DET =:co_st_adl.state OR :co_st_adl.state IS NULL ) AND " +
            "    (V_ATT_DET_L.CREATION_ATTRIBUTE_DETAIL_LIST=:co_st_adl.creation_date OR :co_st_adl.creation_date IS NULL ) AND " +
            "    (V_ATT_DET_L.CREATION_ATTRIBUTE_DETAIL_LIST=:co_st_adl.modify_date OR :co_st_adl.modify_date IS NULL ) AND " +
            "    (V_ATT_DET_L.ID_ATTRIBUTE_VALUE=:att_val.id_attribute_value or :att_val.id_attribute_value IS NULL ) AND " +
            "    (V_ATT_DET_L.ID_ATTRIBUTE=:att_val.id_attribute or :att_val.id_attribute IS NULL  ) AND " +
            "    (V_ATT_DET_L.ID_COMMON=:co_att_val.id_common or :co_att_val.id_common IS NULL  ) AND " +
            "    (V_ATT_DET_L.NAME LIKE :co_att_val.name or :co_att_val.name IS NULL  ) AND " +
            "    (V_ATT_DET_L.DESCRIPTION LIKE :co_att_val.description or :co_att_val.description IS NULL  ) AND" +
            "    (V_ATT_DET_L.ID_STATE=:co_st_att_val.id_common_state OR :co_st_att_val.id_common_state IS NULL ) AND" +
            "    (V_ATT_DET_L.STATE=:co_st_att_val.state OR :co_st_att_val.state IS NULL ) AND" +
            "    (V_ATT_DET_L.CREATION_ATTRIBUTE_VALUE=:co_st_att_val.creation_date OR :co_st_att_val.creation_date IS NULL ) AND" +
            "    (V_ATT_DET_L.MODIFY_ATTRIBUTE_VALUE=:co_st_att_val.modify_date OR :co_st_att_val.modify_date IS NULL )")
    List<AttributeDetailList> read(@BindBean("att_det_l") AttributeDetailList detailList,
                                   @BindBean("co_st_adl")CommonState commonState,
                                   @BindBean("att_val")AttributeValue attributeValue,
                                   @BindBean("co_att_val")Common common,
                                   @BindBean("co_st_att_val")CommonState stateAttVal);

    @SqlQuery("SELECT ID_ATTRIBUTE_DETAIL_LIST FROM ATTRIBUTE_DETAIL_LIST WHERE ID_ATTRIBUTE_DETAIL_LIST IN (SELECT MAX(ID_ATTRIBUTE_DETAIL_LIST) FROM ATTRIBUTE_DETAIL_LIST)")
    Long getPkLast();

    @SqlBatch("INSERT INTO ATTRIBUTE_DETAIL_LIST " +
            "( ID_ATTRIBUTE_LIST, ID_ATTRIBUTE_VALUE, ID_COMMON_STATE) VALUES" +
            "( :id_attribute_list, :id_attribute_valueList, :id_common_stateList )")
    void createBulk(@Bind("id_attribute_list") Long id_attribute_list,@Bind("id_attribute_valueList") List<Long> id_attribute_valueList, @Bind("id_common_stateList")List<Long> id_common_stateList);


    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_ATTRIBUTE_DETAIL_LIST ID,ID_ATTRIBUTE_LIST ID_COMMON, ID_COMMON_STATE FROM ATTRIBUTE_DETAIL_LIST " +
            "  WHERE (ID_ATTRIBUTE_DETAIL_LIST = :id_attribute_detail_list OR :id_attribute_detail_list IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_attribute_detail_list") Long id_attribute_detail_list);

    @SqlUpdate(" UPDATE ATTRIBUTE_DETAIL_LIST SET " +
            "    ID_ATTRIBUTE_LIST = :id_attribute_list, " +
            "    ID_ATTRIBUTE_VALUE=:id_attribute_value " +
            "    WHERE " +
            "            (ID_ATTRIBUTE_DETAIL_LIST =  :id_attribute_detail_list)")
    int update(@Bind("id_attribute_detail_list") Long id_attribute_detail_list,@Bind("id_attribute_list") Long id_attribute_list,@Bind("id_attribute_value") Long id_attribute_value);

    @SqlQuery("SELECT COUNT(ID_ATTRIBUTE_DETAIL_LIST) FROM ATTRIBUTE_DETAIL_LIST WHERE ID_ATTRIBUTE_DETAIL_LIST = :id_atribute_detail_list")
    Integer getValidatorID(@Bind("id_atribute_detail_list") Long id_atribute_detail_list);

}
