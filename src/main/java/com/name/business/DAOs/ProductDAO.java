package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.Product;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.ProductMapper;
import org.skife.jdbi.v2.sqlobject.BindBean;
import com.name.business.representations.ProductDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

import java.util.List;
@RegisterMapper(ProductMapper.class)
public interface ProductDAO {

    @SqlQuery("SELECT * FROM V_PRODUCT PRO " +
            "WHERE " +
            "  (PRO.ID_PRODUCT =:pro.id_product OR :pro.id_product IS  NULL )AND\n" +
            "  (PRO.ID_CATEGORY =:pro.id_category OR :pro.id_category IS NULL )AND\n" +
            "  (PRO.STOCK =:pro.stock OR :pro.stock IS NULL )AND\n" +
            "  (PRO.STOCK_MIN =:pro.stock_min OR  :pro.stock_min IS NULL )AND\n" +
            "  (PRO.IMG_URL =:pro.img_url OR :pro.img_url IS NULL )AND\n" +
            "  (PRO.CODE =:pro.code OR :pro.code IS NULL )AND\n" +
            "  (PRO.ID_TAX =:pro.id_tax OR :pro.id_tax IS NULL )AND\n" +
            "  (PRO.ID_COMMON =:pro.id_common_product OR :pro.id_common_product IS NULL )AND\n" +
            "  (PRO.NAME LIKE:pro.name_product OR :pro.name_product IS NULL )AND\n" +
            "  (PRO.DESCRIPTION LIKE:pro.description_product OR :pro.description_product IS  NULL )AND\n" +
            "  (PRO.ID_STATE =:pro.id_state_product OR :pro.id_state_product IS NULL )AND\n" +
            "  (PRO.STATE =:pro.state_product OR  :pro.state_product IS NULL )AND\n" +
            "  (PRO.CREATION_PRODUCT =:pro.creation_product OR :pro.creation_product IS NULL )AND\n" +
            "  (PRO.MODIFY_PRODUCT=:pro.modify_product OR :pro.modify_product IS  NULL )")
    List<Product> read(@BindBean("pro") Product product);

    //TODO Cambiar string de = por like

    @SqlQuery("SELECT ID_PRODUCT FROM PRODUCT WHERE ID_PRODUCT IN (SELECT MAX(ID_PRODUCT) FROM PRODUCT)")
    Long getPkLast();


    @SqlUpdate("INSERT INTO PRODUCT ( STOCK,STOCK_MIN,IMG_URL,ID_COMMON,ID_CATEGORY,CODE,ID_TAX,ID_COMMON_STATE) " +
            "VALUES (:pro.stock,:pro.stock_min,:pro.img_url,:id_common,:pro.id_category,:pro.code, " +
            "        :pro.id_tax,:id_common_state)")
    void create(@BindBean("pro") ProductDTO productDTO, @Bind("id_common") Long id_common,@Bind("id_common_state") Long id_common_state);

    @SqlQuery("SELECT COUNT(ID_PRODUCT) FROM PRODUCT WHERE ID_PRODUCT = :id_product")
    Integer getValidatorID(@Bind("id_product") Long id_product);


    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_PRODUCT ID,ID_COMMON, ID_COMMON_STATE FROM PRODUCT\n" +
            "  WHERE (ID_PRODUCT = :id_product OR :id_product IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_product") Long id_product);

    @SqlUpdate("UPDATE PRODUCT SET " +
            "    STOCK = :pro.stock, " +
            "    STOCK_MIN = :pro.stock_min," +
            "    IMG_URL = :pro.img_url, " +
            "    ID_CATEGORY = :pro.id_category, " +
            "    CODE = :pro.code, " +
            "    ID_TAX = :pro.id_tax " +
            "    WHERE (ID_PRODUCT =  :id_product)")
    void update( @Bind("id_product") Long id_product, @BindBean("pro") ProductDTO id_inventory);

}
