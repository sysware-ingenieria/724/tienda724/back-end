package com.name.business.DAOs;

import com.name.business.entities.AttributeValue;
import com.name.business.entities.CommonSimple;
import com.name.business.entities.CommonState;
import com.name.business.mappers.AttributeValueMapper;
import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(AttributeValueMapper.class)
public interface AttributeValueDAO {


    @SqlUpdate(" INSERT INTO ATTRIBUTE_VALUE ( ID_ATTRIBUTE,ID_COMMON,ID_COMMON_STATE) VALUES (:id_attribute,:id_common,:id_common_state)")
    void create(@Bind("id_attribute") Long id_attribute,@Bind("id_common") Long id_common, @Bind("id_common_state") Long id_common_state);


    @SqlQuery(" SELECT ID_ATTRIBUTE_VALUE FROM ATTRIBUTE_VALUE WHERE ID_ATTRIBUTE_VALUE IN (SELECT MAX(ID_ATTRIBUTE_VALUE) FROM ATTRIBUTE_VALUE) ")
    Long getPkLast();

    @SqlQuery("SELECT * " +
            "FROM V_ATTRIBUTE_VALUE V_ATT_VAL " +
            "  WHERE " +
            "    (V_ATT_VAL.ID_ATTRIBUTE_VALUE =:att_val.id_attribute_value OR :att_val.id_attribute_value IS NULL ) AND" +
            "    (V_ATT_VAL.ID_ATTRIBUTE=:att_val.id_attribute  OR :att_val.id_attribute IS NULL ) AND" +
            "    (V_ATT_VAL.ID_COMMON=:att_val.id_common_attrib_value OR :att_val.id_common_attrib_value IS NULL ) AND" +
            "    (V_ATT_VAL.NAME LIKE :att_val.name_attrib_value OR :att_val.name_attrib_value IS NULL ) AND" +
            "    (V_ATT_VAL.DESCRIPTION LIKE :att_val.description_attrib_value OR :att_val.description_attrib_value IS NULL ) AND" +
            "    (V_ATT_VAL.ID_STATE=:att_val.id_state_attrib_value OR :att_val.id_state_attrib_value IS NULL ) AND" +
            "    (V_ATT_VAL.STATE=:att_val.state_attrib_value OR :att_val.state_attrib_value IS NULL ) AND" +
            "    (V_ATT_VAL.CREATION_ATTRIBUTE_VALUE=:att_val.creation_attrib_value OR :att_val.creation_attrib_value IS NULL ) AND" +
            "    (V_ATT_VAL.MODIFY_ATTRIBUTE_VALUE=:att_val.modify_attrib_value OR :att_val.modify_attrib_value IS NULL )")
    List<AttributeValue> read(@BindBean("att_val") AttributeValue attributeVal);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_ATTRIBUTE_VALUE ID, ID_COMMON, ID_COMMON_STATE FROM ATTRIBUTE_VALUE\n" +
            "  WHERE (ID_ATTRIBUTE_VALUE = :id_attribute_value OR :id_attribute_value IS NULL) ")
    List<CommonSimple> readCommons(@Bind("id_attribute_value") Long id_attribute_value);



    @SqlUpdate(" UPDATE ATTRIBUTE_VALUE SET " +
            "    ID_ATTRIBUTE = :id_attribute " +
            "    WHERE " +
            "            (ID_ATTRIBUTE_VALUE =  :id_attribute_value)")
    int update(@Bind("id_attribute_value") Long id_attribute_value, @Bind("id_attribute") Long id_attribute);

}
