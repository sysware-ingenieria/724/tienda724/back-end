package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.CategoryMapper;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.representations.CategoryDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import javax.ws.rs.BeanParam;
import java.util.List;

@RegisterMapper(CategoryMapper.class)
public interface CategoryDAO {

    @SqlQuery("SELECT * FROM V_CATEGORY V_CA WHERE\n" +
            "  (V_CA.ID_CATEGORY = :cat.id_category or :cat.id_category is NULL ) AND\n" +
            "  (V_CA.ID_CATEGORY_FATHER = :cat.id_category_father or :cat.id_category_father is NULL) AND\n" +
            "  (V_CA.IMG_URL LIKE :cat.img_url or :cat.img_url is NULL) AND\n" +
            "  (V_CA.ID_THIRD_CATEGORY= :cat.id_third_category or :cat.id_third_category is NULL) AND\n" +
            "  (V_CA.ID_COMMON = :co.id_common or :co.id_common is NULL) AND\n" +
            "  (V_CA.NAME LIKE :co.name or :co.name is NULL) AND\n" +
            "  (V_CA.DESCRIPTION LIKE :co.description or :co.description is NULL) AND\n" +
            "  (V_CA.ID_STATE = :co_st.id_common_state or :co_st.id_common_state is NULL) AND\n" +
            "  (V_CA.ID_STATE = :co_st.state or :co_st.state is NULL) AND\n" +
            "  (V_CA.CREATION_CATEGORY = :co_st.creation_date or :co_st.creation_date is NULL) AND\n" +
            "  (V_CA.MODIFY_CATEGORY = :co_st.modify_date or :co_st.modify_date is NULL)")
    List<Category> read(@BindBean("cat")Category category, @BindBean("co") Common common, @BindBean("co_st") CommonState commonState);

    @SqlQuery("SELECT ID_CATEGORY FROM CATEGORY WHERE ID_CATEGORY IN (SELECT MAX(ID_CATEGORY) FROM CATEGORY)")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_CATEGORY) FROM CATEGORY WHERE ID_CATEGORY = :id_category")
    Integer getValidatorID(@Bind("id_category") Long id_category);

    @SqlUpdate("INSERT INTO CATEGORY \n" +
            "( ID_COMMON,IMG_URL,ID_CATEGORY_FATHER,ID_THIRD,ID_COMMON_STATE)\n" +
            "VALUES (:id_common,:cat.img_url,:cat.id_category_father,:cat.id_third_category,:id_state)")
    void create(@BindBean("cat") CategoryDTO categoryDTO, @Bind("id_common") Long id_common, @Bind("id_state") Long id_common_state);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_CATEGORY ID,ID_COMMON, ID_COMMON_STATE FROM CATEGORY " +
            "WHERE (ID_CATEGORY = :id_category OR :id_category IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_category") Long id_category);

    @SqlUpdate("UPDATE CATEGORY SET IMG_URL = :cat.img_url, ID_CATEGORY_FATHER = :cat.id_category_father, ID_THIRD=:cat.id_third_category \n" +
            "WHERE ID_CATEGORY = :id_category")
    void update(@Bind("id_category") Long id_category, @BindBean("cat") CategoryDTO categoryDTO);
}
