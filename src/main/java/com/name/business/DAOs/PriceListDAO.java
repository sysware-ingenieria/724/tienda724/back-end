package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.PriceList;
import com.name.business.entities.Product;

import com.name.business.mappers.PriceListMapper;
import com.name.business.representations.PriceListDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;


import java.util.List;

@RegisterMapper(PriceListMapper.class)

public interface PriceListDAO {

    @SqlQuery("SELECT ID_PRICE_LIST FROM PRICE_LIST WHERE ID_PRICE_LIST IN (SELECT MAX(ID_PRICE_LIST) FROM PRICE_LIST)")
    Long getPkLast();

    @SqlQuery("SELECT * FROM V_PRICE_LIST V_PR_LIST\n" +
            "WHERE\n" +
            "  ( V_PR_LIST.ID_PRICE_LIST =:pr_list.id_price_list OR :pr_list.id_price_list IS NULL) AND\n" +
            "  ( V_PR_LIST.ID_PRODUCT_THIRD =:pr_list.id_product_third_pr_list OR :pr_list.id_product_third_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.STANDARD_PRICE =:pr_list.standard_price OR :pr_list.standard_price IS NULL) AND\n" +
            "  ( V_PR_LIST.MIN_PRICE =:pr_list.min_price OR :pr_list.min_price IS NULL) AND\n" +
            "  ( V_PR_LIST.ID_COMMON =:pr_list.id_common_pr_list OR :pr_list.id_common_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.NAME LIKE :pr_list.name_pr_list OR :pr_list.name_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.DESCRIPTION LIKE :pr_list.description_pr_list OR :pr_list.description_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.ID_STATE =:pr_list.id_state_pr_list OR :pr_list.id_state_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.STATE =:pr_list.state_pr_list OR :pr_list.state_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.CREATION_PRICE_LIST =:pr_list.creation_pr_list OR :pr_list.creation_pr_list IS NULL) AND\n" +
            "  ( V_PR_LIST.MODIFY_PRICE_LIST =:pr_list.modify_pr_list OR :pr_list.modify_pr_list IS NULL)")
    List<PriceList> read(@BindBean("pr_list") PriceList priceList);



    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_PRICE_LIST ID,ID_COMMON, ID_COMMON_STATE FROM PRICE_LIST " +
            "WHERE (ID_PRICE_LIST= :id_price_list OR :id_price_list IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_price_list") Long id_price_list);

    @SqlQuery("SELECT COUNT(ID_PRICE_LIST) FROM PRICE_LIST WHERE ID_PRICE_LIST = :id_price_list")
    Integer getValidatorID(@Bind("id_price_list") Long id_price_list);

    @SqlUpdate("INSERT INTO PRICE_LIST ( ID_PRODUCT_THIRD,ID_COMMON,STANDARD_PRICE,MIN_PRICE,ID_COMMON_STATE) VALUES\n" +
            "  (:pr_list.id_product_third,:id_common,:pr_list.standard_price,:pr_list.min_price,:id_state)")
    void create(@BindBean("pr_list") PriceListDTO PriceListDTO, @Bind("id_common") Long id_common, @Bind("id_state") Long id_state);


   @SqlUpdate("UPDATE PRICE_LIST SET STANDARD_PRICE =:pr_list.standard_price,\n" +
           "  MIN_PRICE =:pr_list.min_price\n" +
           "WHERE (ID_PRICE_LIST =  :id_price_list)")
    void update(@Bind("id_price_list") Long id_price_list, @BindBean("pr_list") PriceListDTO priceListDTO);

    @SqlQuery("SELECT COUNT(ID_PRODUCT_THIRD) FROM PRODUCT_THIRD WHERE ID_PRODUCT_THIRD = :id_product_third")
    Integer getValidatorID_product_third(@Bind("id_product_third") Long id_product_third);

}
