package com.name.business.businesses;

import com.name.business.DAOs.AttributeDAO;
import com.name.business.entities.Attribute;
import com.name.business.entities.CommonSimple;
import com.name.business.representations.AttributeDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.attributeSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;


public class AttributeBusiness {

    private AttributeDAO attributeDAO;
    private CommonBusiness  commonBusiness;
    private CommonStateBusiness commonStateBusiness;

    public AttributeBusiness(AttributeDAO attributeDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness) {
        this.attributeDAO = attributeDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }
    /**
     *
     * @param attribute
     *
     * @return
     */
    public Either<IException, List<Attribute>> getAttributes(Attribute attribute) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Attribute ||||||||||||||||| ");
            Attribute attributeSanitation = attributeSanitation(attribute);
            return Either.right(attributeDAO.read(attributeSanitation,attributeSanitation.getCommon(),attributeSanitation.getState()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException,Long> createAttribute(AttributeDTO attributeDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_attribute = null;
        Long id_common_state = null;
        System.out.println(attributeDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Attibute  ||||||||||||||||| ");
            if (attributeDTO!=null){

                if (attributeDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(attributeDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (attributeDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(attributeDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }
                attributeDAO.create(formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_attribute = attributeDAO.getPkLast();

                return Either.right(id_attribute);
            }else{
                msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_attribute
     * @param attributeDTO
     * @return
     */
    public Either<IException, Long> updateAttribute(Long id_attribute, AttributeDTO attributeDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Attribute currentAttribute= null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Attribute ||||||||||||||||| ");
            if ((id_attribute != null && id_attribute > 0) && attributeDTO!=null) {

                List<CommonSimple> readCommons = attributeDAO.readCommons(formatoLongSql(id_attribute));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), attributeDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), attributeDTO.getStateDTO());


                // Attribute update
                //attributeDAO.update(id_attribute,attributeDTO);

                return Either.right(id_attribute);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_attribute
     * @return
     */
    public Either<IException, Long> deleteAttribute(Long id_attribute) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete person ||||||||||||||||| ");
            if (id_attribute != null && id_attribute > 0) {
                List<CommonSimple> readCommons = attributeDAO.readCommons(formatoLongSql(id_attribute));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common

                return Either.right(id_attribute);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = attributeDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }


}
