package com.name.business.businesses;

import com.name.business.DAOs.AttributeDetailListDAO;
import com.name.business.entities.*;
import com.name.business.representations.AttributeDetailListDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.attributeDeatilListSanitation;
import static com.name.business.sanitations.EntitySanitation.attributeSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class AttributeDetailListBusiness {

    private AttributeDetailListDAO attributeDetailListDAO;
    private AttributeListBusiness attributeListBusiness;
    private AttributeValueBusiness attributeValueBusiness;
    private CommonStateBusiness commonStateBusiness;

    public AttributeDetailListBusiness(AttributeDetailListDAO attributeDetailListDAO, AttributeListBusiness attributeListBusiness,
                                       AttributeValueBusiness attributeValueBusiness, CommonStateBusiness commonStateBusiness) {
        this.attributeDetailListDAO = attributeDetailListDAO;
        this.attributeListBusiness = attributeListBusiness;
        this.attributeValueBusiness = attributeValueBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }


    public Either<IException,Long> createAttributeDetailList(Long id_attribute_list,List<AttributeDetailListDTO> attributeDetailListDTOList){
        List<String> msn = new ArrayList<>();
        List<Long> id_common_stateList = new ArrayList<>();
        List<Long>id_attribute_valueList= new ArrayList<>();

        System.out.println(attributeDetailListDAO.getPkLast());
        try {

            System.out.println("|||||||||||| Starting creation Attribute detail list ||||||||||||||||| ");
            if(formatoLongSql(id_attribute_list)==null){
                msn.add("The Query Param id attribute_list is necessary, probably it was bad or was value null ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if ((attributeDetailListDTOList!=null || attributeDetailListDTOList.size()>0)){


                //TODO validate the  id_attribute_list if exist on database a register with this id
                if (attributeListBusiness.validatorID(id_attribute_list).equals(false)){
                    msn.add("It id_attribute_list does not exist register on  Attibute List, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                for (AttributeDetailListDTO attribDetailListDTO: attributeDetailListDTOList) {

                    if (attribDetailListDTO.getStateDTO() == null) {
                        attribDetailListDTO.setStateDTO(new CommonStateDTO(1,new Date(),new Date()));

                    }

                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(attribDetailListDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_stateList.add(commonThirdEither.right().value());
                    }
                    id_attribute_valueList.add(attribDetailListDTO.getId_attribute_value());
                }

                attributeDetailListDAO.createBulk(formatoLongSql(id_attribute_list),id_attribute_valueList,id_common_stateList);

                return Either.right(id_attribute_list);
            }else{
                msn.add("It does not recognize Attibute detail list, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_attribute_list
     * @param attributeDetailListDTO
     * @return
     */
    public Either<IException, Long> updateAttributeDetailList(Long id_attribute_detail_list,Long id_attribute_list, AttributeDetailListDTO attributeDetailListDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        AttributeDetailList actualAttributeDetailList=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Attribute Detail List||||||||||||||||| ");
            if ((id_attribute_detail_list != null && id_attribute_detail_list > 0) ) {
                AttributeDetailList attributeDetailList = new AttributeDetailList(
                        id_attribute_detail_list, null, new CommonState(null, null, null, null),
                        new AttributeValue(null, null, new Common(null, null, null),
                                new CommonState(null, null, null, null)));


                List<AttributeDetailList> read = attributeDetailListDAO.read(attributeDetailList,attributeDetailList.loadingCommonState(),attributeDetailList.getValue(),
                        attributeDetailList.getValue().loadingCommon(),attributeDetailList.getValue().loadingCommonState());

                if (read.size()<=0){
                    msn.add("It does not recognize ID Attribute Value, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (id_attribute_list == null || id_attribute_list<1) {
                    id_attribute_list=actualAttributeDetailList.getId_attribute_list();
                } else {
                    id_attribute_list=formatoLongSql(id_attribute_list);
                }
                //TODO validate the  id attribute list if exist on database a register with this id
                if (attributeListBusiness.validatorID(id_attribute_list).equals(false)){
                    msn.add("It ID Attribute List does not exist register on  Attribute List, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (attributeDetailListDTO.getId_attribute_value() == null || attributeDetailListDTO.getId_attribute_value()<1) {
                    attributeDetailListDTO.setId_attribute_value(actualAttributeDetailList.getValue().getId_attribute_value());
                } else {
                    attributeDetailListDTO.setId_attribute_value(formatoLongSql(attributeDetailListDTO.getId_attribute_value()));
                }




                List<CommonSimple> readCommons = attributeDetailListDAO.readCommons(formatoLongSql(id_attribute_detail_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }





                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), attributeDetailListDTO.getStateDTO());




                // Attribute update
                attributeDetailListDAO.update(id_attribute_detail_list,id_attribute_list,attributeDetailListDTO.getId_attribute_value());

                return Either.right(id_attribute_detail_list);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param attributeDetailList
     *
     * @return
     */
    public Either<IException, List<AttributeDetailList>> getAttributeDetailLists(AttributeDetailList attributeDetailList) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Attribute ||||||||||||||||| ");
            AttributeDetailList attributeDetailListSanitation = attributeDeatilListSanitation(attributeDetailList);
            return Either.right(attributeDetailListDAO.read(attributeDetailListSanitation,
                    attributeDetailListSanitation.loadingCommonState(),attributeDetailListSanitation.getValue(),
                    attributeDetailListSanitation.getValue().loadingCommon(),
                    attributeDetailListSanitation.getValue().loadingCommonState()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_attribute_detail_list
     * @return
     */
    public Either<IException, Long> deleteAttributeDetail(Long id_attribute_detail_list) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Attribute Detail List ||||||||||||||||| ");
            if (id_attribute_detail_list != null && id_attribute_detail_list > 0) {
                List<CommonSimple> readCommons = attributeDetailListDAO.readCommons(formatoLongSql(id_attribute_detail_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_attribute_detail_list);
            } else {
                msn.add("It does not recognize ID Attribute Detail List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = attributeDetailListDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }


}
