package com.name.business.businesses;

import com.name.business.DAOs.PriceListDAO;
import com.name.business.entities.Common;
import com.name.business.entities.CommonSimple;
import com.name.business.entities.CommonState;
import com.name.business.entities.PriceList;
import com.name.business.representations.PriceListDTO;
import com.name.business.representations.ProductDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.priceListSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class PriceListBusiness {
    private PriceListDAO priceListDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;

    public PriceListBusiness(PriceListDAO priceListDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness) {
        this.priceListDAO = priceListDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }


    /**
     *
     * @param priceList
     *
     * @return
     */
    public Either<IException, List<PriceList>> getPriceList(PriceList priceList) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults taxtariff ||||||||||||||||| ");
            PriceList taxTariffSanitation = priceListSanitation(priceList);
            return Either.right(priceListDAO.read(taxTariffSanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_price_list
     * @return
     */
    public Either<IException, Long> deleteProduct(Long id_price_list) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Price  List ||||||||||||||||| ");
            if (id_price_list != null && id_price_list > 0) {

                //TODO validate the  id_category if exist on database a register with this id
                if (validatorID(id_price_list).equals(false)){
                    msn.add("It ID Price  does not exist register on  Price, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = priceListDAO.readCommons(formatoLongSql(id_price_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_price_list);
            } else {
                msn.add("It does not recognize ID Price List , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = priceListDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID_product_third(Long id) {
        Integer validator = 0;
        try {
            validator = priceListDAO.getValidatorID_product_third(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }


    public Either<IException,Long> createProduct(PriceListDTO priceListDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_product = null;
        Long id_common_state = null;
        System.out.println(priceListDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Price List  ||||||||||||||||| ");
            if (priceListDTO!=null){

                //TODO validate the  id_category if exist on database a register with this id
                if (validatorID_product_third(priceListDTO.getId_product_third()).equals(false)){
                    msn.add("It ID Product Third does not exist register on  Product Third, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (priceListDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(priceListDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (priceListDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(priceListDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }



                priceListDAO.create(priceListDTO,formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_product = priceListDAO.getPkLast();

                return Either.right(id_product);
            }else{
                msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_price_list
     * @param priceListDTO
     * @return
     */
    public Either<IException, Long> updateProduct(Long id_price_list, PriceListDTO priceListDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        PriceList actualPriceList=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Product ||||||||||||||||| ");
            if ((id_price_list != null && id_price_list > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID_product_third(id_price_list).equals(false)){
                    msn.add("It ID Product Third does not exist register on  Product Third, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<PriceList> read = priceListDAO.read(
                        new PriceList(id_price_list, null,null,null, new Common(null,null,null),
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    msn.add("It does not recognize ID Product , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = priceListDAO.readCommons(formatoLongSql(id_price_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), priceListDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), priceListDTO.getStateDTO());

                actualPriceList = read.get(0);
                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (priceListDTO.getId_product_third() == null || priceListDTO.getId_product_third()<1) {
                    priceListDTO.setId_product_third(actualPriceList.getId_product_third_pr_list());
                } else {
                    priceListDTO.setId_product_third(formatoLongSql(priceListDTO.getId_product_third()));
                }


                // Attribute update
                priceListDAO.update(id_price_list,priceListDTO);

                return Either.right(id_price_list);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
