package com.name.business.businesses;

import com.name.business.DAOs.InventoryDAO;
import com.name.business.entities.*;
import com.name.business.representations.CommonStateDTO;
import com.name.business.representations.InventoryDTO;
import com.name.business.representations.InventoryDetailDTO;
import com.name.business.representations.InventoryQuantityDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Transaction;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.inventorySanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class InventoryBusiness {
    private InventoryDAO inventoryDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;
    private InventoryDetailBusiness inventoryDetailBusiness;

    public InventoryBusiness(InventoryDAO inventoryDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness,
                             InventoryDetailBusiness inventoryDetailBusiness) {
        this.inventoryDAO = inventoryDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
        this.inventoryDetailBusiness = inventoryDetailBusiness;
    }

    public Either<IException,Long> createInventory(InventoryDTO inventoryDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_common_state = null;
        Long id_inventory = null;
        System.out.println(inventoryDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Inventory  ||||||||||||||||| ");
            if (inventoryDTO!=null){

                if (inventoryDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(inventoryDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (inventoryDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(inventoryDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }
                inventoryDAO.create(formatoLongSql(inventoryDTO.getId_third()),formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_inventory = inventoryDAO.getPkLast();

                if (inventoryDTO.getDetailDTOS() != null && inventoryDTO.getDetailDTOS().size()>0 && id_inventory>0) {
                    Either<IException, Long> commonThirdEither = inventoryDetailBusiness.createInventoryDetail(id_inventory,inventoryDTO.getDetailDTOS());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }

                return Either.right(id_inventory);
            }else{
                msn.add("It does not recognize Inventory, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_inventory
     * @param inventoryDTO
     * @return
     */
    public Either<IException, Long> updateInventary(Long id_inventory, InventoryDTO inventoryDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Inventory actualInventary=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Inventory ||||||||||||||||| ");
            if ((id_inventory != null && id_inventory > 0)) {

                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_inventory).equals(false)){
                    msn.add("It ID Inventory does not exist register on  Inventory Value, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<Inventory> read = inventoryDAO.read(new Inventory(id_inventory,null,
                        new Common(null,null,null),
                        new CommonState(null,null,null,null)));

                if (read.size()<=0){
                    msn.add("It does not recognize ID Inventory, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = inventoryDAO.readCommons(formatoLongSql(id_inventory));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), inventoryDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), inventoryDTO.getStateDTO());

                actualInventary = read.get(0);
                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (inventoryDTO.getId_third() == null || inventoryDTO.getId_third()<1) {
                    inventoryDTO.setId_third(actualInventary.getId_third());
                } else {
                    inventoryDTO.setId_third(formatoLongSql(inventoryDTO.getId_third()));
                }
                // Inventory update
                inventoryDAO.update(id_inventory,inventoryDTO.getId_third());

                return Either.right(id_inventory);

            } else {
                msn.add("It does not recognize ID Inventory, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param inventory
     *
     * @return
     */
    public Either<IException, List<Inventory>> getInventories(Inventory inventory) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Inventories  ||||||||||||||||| ");
            Inventory inventorySanitation = inventorySanitation(inventory);
            return Either.right(inventoryDAO.read(inventorySanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_inventory
     * @return
     */
    public Either<IException, Long> deleteInventory(Long id_inventory) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Inventory ||||||||||||||||| ");
            if (id_inventory != null && id_inventory > 0) {
                List<CommonSimple> readCommons = inventoryDAO.readCommons(formatoLongSql(id_inventory));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                Either<IException, Long> deleteCommonStateEither = commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common

                // TODO Now delete all your inventory detail
                Either<IException, Long> deleteInventoryDetail=inventoryDetailBusiness.deleteInventoryDetail(null,id_inventory);

                return Either.right(id_inventory);
            } else {
                msn.add("It does not recognize ID Inventory, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = inventoryDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    @Transaction
    public Either<IException, String> discountInventary(Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        List<InventoryDetailDTO> inventoryDetailDTOList= new ArrayList<>();
        try {
            if (!validatorID(id_inventory)){
                return Either.left(new BussinessException("It ID Inventory does not exist register on Inventory, probably it has bad"));
            }
            if (id_inventory!=null && id_inventory>0){
                if (inventoryQuantityDTOList!=null && inventoryQuantityDTOList.size()>0){
                    Either<IException, List<InventoryDetailSimple>> loadingDeatilInventoryEither = loadingDetailInventory(id_inventory, inventoryQuantityDTOList,IS_DISCOUNT);
                    if (loadingDeatilInventoryEither.isRight()){
                        List<InventoryDetailSimple> inventoryDetailSimples = loadingDeatilInventoryEither.right().value();
                        for (InventoryDetailSimple  detailSimple:inventoryDetailSimples){

                            inventoryDetailDTOList.add(new InventoryDetailDTO(
                                    detailSimple.getId_inventory_detail(),detailSimple.getId_product_third(),
                                    detailSimple.getQuantity(),detailSimple.getCode(),
                                    new CommonStateDTO(detailSimple.getState_inv_detail(),detailSimple.getCreation_inv_detail(),detailSimple.getModify_inv_detail())
                                    ));
                        }

                        if (inventoryDetailDTOList.size()>0){
                            System.out.println("------------ INICIA PROCESO DE DESCARGA DEL INVENARIO -------------");
                            Either<IException, Long> updateInventaryDetailEither = inventoryDetailBusiness.updateInventaryDetail(id_inventory, inventoryDetailDTOList);
                            if (updateInventaryDetailEither.isRight()){

                                return Either.right("Inventory download was successful");

                            }else{
                                return Either.left(updateInventaryDetailEither.left().value());
                            }

                        }else{

                            msn.add("Inventory download was interrupted");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        return Either.left(loadingDeatilInventoryEither.left().value());
                    }

                }else{
                    msn.add("The Product Third is empty");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }
            }else{
                msn.add("The ID Inventory  is necessary ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<InventoryDetailSimple>> loadingDetailInventory(Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList, Boolean is_discount) {
        List<String> msn = new ArrayList<>();
        List<InventoryDetailSimple> inventoryDetailList= new ArrayList<>();
        try {
            if (!validatorID(id_inventory)){
                return Either.left(new BussinessException("It ID Inventory does not exist register on Inventory, probably it has bad"));
            }
            if (inventoryQuantityDTOList!=null && inventoryQuantityDTOList.size()>0){
                for (InventoryQuantityDTO quantityDTO:inventoryQuantityDTOList){
                    Either<IException, List<InventoryDetailSimple>> inventoryDetailSimpleEither = inventoryDetailBusiness.getInventoryDetailSimple(
                            new InventoryDetailSimple(
                                    quantityDTO.getId_inventory_detail(),id_inventory,quantityDTO.getId_product_third(),null,null,
                                    new CommonState(null,null,null,null)
                                    )
                    );

                    if (inventoryDetailSimpleEither.isRight() && inventoryDetailSimpleEither.right().value().size()>0){
                        List<InventoryDetailSimple> detailSimples = inventoryDetailSimpleEither.right().value();
                        if (detailSimples.size()==1){

                            InventoryDetailSimple inventoryDetailSimple = detailSimples.get(0);

                            if (is_discount){ // It if is for do a discount

                                if ( inventoryDetailSimple.getQuantity()!=null && inventoryDetailSimple.getQuantity()>0 && (inventoryDetailSimple.getQuantity()-quantityDTO.getQuantity())>=0){
                                    inventoryDetailSimple.setQuantity(inventoryDetailSimple.getQuantity()-quantityDTO.getQuantity());
                                    inventoryDetailList.add(inventoryDetailSimple);

                                }else{
                                    msn.add("The Product Third "+quantityDTO.getId_product_third()+" with detail inventory "+
                                            inventoryDetailSimple.getId_inventory_detail()+" does not have enough units");
                                    return Either.left(new BussinessException(messages_errors(msn)));
                                }
                            }else{
                                if ( inventoryDetailSimple.getQuantity()!=null && inventoryDetailSimple.getQuantity()>0){
                                    inventoryDetailSimple.setQuantity(inventoryDetailSimple.getQuantity()+quantityDTO.getQuantity());
                                    inventoryDetailList.add(inventoryDetailSimple);

                                }else{
                                    inventoryDetailSimple.setQuantity(quantityDTO.getQuantity());
                                    inventoryDetailList.add(inventoryDetailSimple);
                                }

                            }



                        }else {
                            msn.add("The Product Third "+quantityDTO.getId_product_third()+" has more than one match");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                        inventoryDetailList.addAll(inventoryDetailSimpleEither.right().value());

                    }else{
                        msn.add("The Product Third "+quantityDTO.getId_product_third()+" doesn't exist on inventory");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }
                }
                return Either.right(inventoryDetailList);

            }else {
                msn.add("The Product Third is empty");
                return Either.left(new BussinessException(messages_errors(msn)));

            }

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, String> plusInventory(Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        List<InventoryDetailDTO> inventoryDetailDTOList= new ArrayList<>();
        try {
            if (id_inventory!=null && id_inventory>0){
                if (inventoryQuantityDTOList!=null && inventoryQuantityDTOList.size()>0){
                    Either<IException, List<InventoryDetailSimple>> loadingDeatilInventoryEither = loadingDetailInventory(id_inventory, inventoryQuantityDTOList,!IS_DISCOUNT);
                    if (loadingDeatilInventoryEither.isRight()){
                        List<InventoryDetailSimple> inventoryDetailSimples = loadingDeatilInventoryEither.right().value();
                        for (InventoryDetailSimple  detailSimple:inventoryDetailSimples){

                            inventoryDetailDTOList.add(new InventoryDetailDTO(
                                    detailSimple.getId_inventory_detail(),detailSimple.getId_product_third(),
                                    detailSimple.getQuantity(),detailSimple.getCode(),
                                    new CommonStateDTO(detailSimple.getState_inv_detail(),detailSimple.getCreation_inv_detail(),detailSimple.getModify_inv_detail())
                            ));
                        }

                        if (inventoryDetailDTOList.size()>0){
                            System.out.println("------------ INICIA PROCESO DE CARGA DEL INVENARIO -------------");
                            Either<IException, Long> updateInventaryDetailEither = inventoryDetailBusiness.updateInventaryDetail(id_inventory, inventoryDetailDTOList);
                            if (updateInventaryDetailEither.isRight()){

                                return Either.right("Inventory load was successful");

                            }else{
                                return Either.left(updateInventaryDetailEither.left().value());
                            }

                        }else{

                            msn.add("Inventory load was interrupted");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        return Either.left(loadingDeatilInventoryEither.left().value());
                    }

                }else{
                    msn.add("The Product Third is empty");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }
            }else{
                msn.add("The ID Inventory  is necessary ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
