package com.name.business.businesses;

import com.name.business.DAOs.TaxTariffDAO;
import com.name.business.entities.*;
import com.name.business.representations.PriceListDTO;
import com.name.business.representations.TaxTariffDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.taxTariffSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class TaxTariffBusiness {

    private TaxTariffDAO taxTariffDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;


    public TaxTariffBusiness(TaxTariffDAO taxTariffDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness) {
        this.taxTariffDAO = taxTariffDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }

    /**
     *
     * @param taxTariff
     *
     * @return
     */
    public Either<IException, List<TaxTariff>> getProduct(TaxTariff taxTariff) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults taxtariff ||||||||||||||||| ");
            TaxTariff taxTariffSanitation = taxTariffSanitation(taxTariff);
            return Either.right(taxTariffDAO.read(taxTariffSanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_taxt_tariff
     * @return
     */
    public Either<IException, Long> deleteProduct(Long id_taxt_tariff) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Price  List ||||||||||||||||| ");
            if (id_taxt_tariff != null && id_taxt_tariff > 0) {

                //TODO validate the  id_category if exist on database a register with this id
                if (validatorID(id_taxt_tariff).equals(false)){
                    msn.add("It ID Price  does not exist register on  Price, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = taxTariffDAO.readCommons(formatoLongSql(id_taxt_tariff));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_taxt_tariff);
            } else {
                msn.add("It does not recognize ID Price List , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = taxTariffDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException,Long> createProduct(TaxTariffDTO taxTariffDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_product = null;
        Long id_common_state = null;
        System.out.println(taxTariffDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Price List  ||||||||||||||||| ");
            if (taxTariffDTO!=null){

                if (taxTariffDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(taxTariffDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (taxTariffDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(taxTariffDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }



                taxTariffDAO.create(taxTariffDTO,formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_product = taxTariffDAO.getPkLast();

                return Either.right(id_product);
            }else{
                msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_price_list
     * @param taxTariffDTO
     * @return
     */
    public Either<IException, Long> updateTaxtTariff(Long id_price_list, TaxTariffDTO taxTariffDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        TaxTariff actualTariff=null;

        try {
            System.out.println("|||||||||||| Starting update Product ||||||||||||||||| ");
            if ((id_price_list != null && id_price_list > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_price_list).equals(false)){
                    msn.add("It ID Product Third does not exist register on  Product Third, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<TaxTariff> read = taxTariffDAO.read(
                        new TaxTariff(id_price_list, null, new Common(null,null,null),
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    msn.add("It does not recognize ID Product , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = taxTariffDAO.readCommons(formatoLongSql(id_price_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), taxTariffDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), taxTariffDTO.getStateDTO());

                // Attribute update
                taxTariffDAO.update(id_price_list,taxTariffDTO);

                return Either.right(id_price_list);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
