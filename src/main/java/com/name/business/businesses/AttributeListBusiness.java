package com.name.business.businesses;

import com.name.business.DAOs.AttributeListDAO;
import com.name.business.DAOs.AttributeValueDAO;
import com.name.business.entities.Attribute;
import com.name.business.entities.AttributeList;
import com.name.business.entities.CommonSimple;
import com.name.business.representations.AttributeDTO;
import com.name.business.representations.AttributeListDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.attributeDeatilListSanitation;
import static com.name.business.sanitations.EntitySanitation.attributeListSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class AttributeListBusiness {

    private AttributeListDAO attributeListDAO;
    private CommonStateBusiness commonStateBusiness;

    public AttributeListBusiness(AttributeListDAO attributeListDAO, CommonStateBusiness commonStateBusiness) {
        this.attributeListDAO = attributeListDAO;
        this.commonStateBusiness = commonStateBusiness;
    }


    public Either<IException,Long> createAttributeList(AttributeListDTO attributeListDTO){
        List<String> msn = new ArrayList<>();
        Long id_attribute_value = null;
        Long id_common_state = null;
        System.out.println(attributeListDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Attribute List ||||||||||||||||| ");
            if (attributeListDTO!=null){



                if (attributeListDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(attributeListDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }
                attributeListDAO.create(formatoLongSql(id_common_state));
                id_attribute_value = attributeListDAO.getPkLast();

                return Either.right(id_attribute_value);
            }else{
                msn.add("It does not recognize Attibute List, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param attributeList
     *
     * @return
     */
    public Either<IException, List<AttributeList>> getAttributeLists(AttributeList attributeList) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Attribute ||||||||||||||||| ");
            AttributeList attributeDetailListSanitation = attributeListSanitation(attributeList);
            return Either.right(attributeListDAO.read(attributeDetailListSanitation,attributeDetailListSanitation.getState()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_attribute_list
     * @param attributeListDTO
     * @return
     */
    public Either<IException, Long> updateAttributeList(Long id_attribute_list, AttributeListDTO attributeListDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Attribute currentAttribute= null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Attribute ||||||||||||||||| ");
            if ((id_attribute_list != null && id_attribute_list > 0) && attributeListDTO!=null) {

                List<CommonSimple> readCommons = attributeListDAO.readCommons(formatoLongSql(id_attribute_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), attributeListDTO.getStateDTO());

                return Either.right(id_attribute_list);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
    /**
     * @param id 
     * @return  [true] if exist id or [false] this id does not exist on database
     * */
    public Boolean validatorID(Long id) {
        try {
            if (attributeListDAO.getValidatorID(id) > 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }


    /**
     * @param id_attribute_detail_list
     * @return
     */
    public Either<IException, Long> deleteAttributeList(Long id_attribute_detail_list) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Attribute  List ||||||||||||||||| ");
            if (id_attribute_detail_list != null && id_attribute_detail_list > 0) {
                List<CommonSimple> readCommons = attributeListDAO.readCommons(formatoLongSql(id_attribute_detail_list));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_attribute_detail_list);
            } else {
                msn.add("It does not recognize ID Attribute List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
