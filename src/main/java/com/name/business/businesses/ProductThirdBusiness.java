package com.name.business.businesses;

import com.name.business.DAOs.ProductThirdDAO;
import com.name.business.entities.*;
import com.name.business.representations.ProductThirdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.productThirdSanitation;
import static com.name.business.sanitations.EntitySanitation.productThirdSimpleSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class ProductThirdBusiness {

    private ProductThirdDAO productThirdDAO;
    private ProductBusiness productBusiness;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;
    private MeasureUnitBusiness measureUnitBusiness;
    private AttributeListBusiness attributeListBusiness;
    private CategoryBusiness categoryBusiness;


    public ProductThirdBusiness(ProductThirdDAO productThirdDAO, ProductBusiness productBusiness, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness, MeasureUnitBusiness measureUnitBusiness, AttributeListBusiness attributeListBusiness, CategoryBusiness categoryBusiness) {
        this.productThirdDAO = productThirdDAO;
        this.productBusiness = productBusiness;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
        this.measureUnitBusiness = measureUnitBusiness;
        this.attributeListBusiness = attributeListBusiness;
        this.categoryBusiness = categoryBusiness;
    }

    public Either<IException, Long> createProductThird(ProductThirdDTO productThirdDTO) {
        List<String> msn = new ArrayList<>();
        Long id_product = null;
        Long id_common_state = null;

        try {

            System.out.println("|||||||||||| Starting creation Product Third  ||||||||||||||||| ");
            if (productThirdDTO!=null){

                //TODO validate the  id_product if exist on database a register with this id
                if (productBusiness.validatorID(productThirdDTO.getId_product()).equals(false)){
                    msn.add("It ID Product does not exist register on  Product, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                //TODO validate the  id_measure_unit  if exist on database a register with this id
                if (measureUnitBusiness.validatorID(productThirdDTO.getId_measure_unit()).equals(false)){
                    msn.add("It ID Measure Unit does not exist register on  Measure Unit, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (categoryBusiness.validatorID(productThirdDTO.getId_category_third()).equals(false)){
                    msn.add("It ID Category does not exist register on  Mea, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                if (productThirdDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(productThirdDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }

                productThirdDAO.create(productThirdDTO,formatoLongSql(id_common_state));
                id_product = productThirdDAO.getPkLast();

                return Either.right(id_product);
            }else{
                msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = productThirdDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException, List<ProductThirdSimple>> getProductThirdSimple(ProductThirdSimple productThirdSimple) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Product Third Simple ||||||||||||||||| ");
            ProductThirdSimple productThirdSimpleSanitation = productThirdSimpleSanitation(productThirdSimple);
            return Either.right(productThirdDAO.readSimple(productThirdSimpleSanitation, productThirdSimpleSanitation.getMeasure_unit()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<ProductThird>> getProductThird(ProductThird productThird) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Product Third ||||||||||||||||| ");
            ProductThird productThirdSanitation = productThirdSanitation(productThird);
            return Either.right(productThirdDAO.read(productThirdSanitation.getDescription(), productThirdSanitation.getDescription().getMeasure_unit(),
                    productThirdSanitation.getProduct()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_product_third
     * @return
     */
    public Either<IException, Long> deleteProductThird(Long id_product_third) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Attribute  List ||||||||||||||||| ");
            if (id_product_third != null && id_product_third > 0) {
                List<CommonSimple> readCommons = productThirdDAO.readCommons(formatoLongSql(id_product_third));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_product_third);
            } else {
                msn.add("It does not recognize ID Attribute List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_product_third
     * @param productThirdDTO
     * @return
     */
    public Either<IException, Long> updateProductThird(Long id_product_third, ProductThirdDTO productThirdDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        ProductThirdSimple actualProductThirdSimple =null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Product ||||||||||||||||| ");
            if ((id_product_third != null && id_product_third > 0)) {

                //TODO validate the  id attribute if exist on database a register with this id
                if (productBusiness.validatorID(productThirdDTO.getId_product()).equals(false)){
                    msn.add("It ID Product Third does not exist register on  Product third , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (measureUnitBusiness.validatorID(productThirdDTO.getId_measure_unit()).equals(false)){
                    msn.add("It ID Measure Unit does not exist register on  Measure Unit , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (attributeListBusiness.validatorID(productThirdDTO.getId_attribute_list()).equals(false)){
                    msn.add("It ID Attribute List does not exist register on  Attribute List , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                if (categoryBusiness.validatorID(productThirdDTO.getId_category_third()).equals(false)){
                    msn.add("It ID Category does not exist register on  Category, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                ProductThirdSimple productThirdSimple = new ProductThirdSimple(id_product_third, null, null, null, null,null,null,
                        new CommonState(null, null, null, null),
                        new MeasureUnit(null, null,
                                new Common(null, null, null),
                                new CommonState(null, null, null, null)
                        ), null, null
                );


                List<ProductThirdSimple> read = productThirdDAO.readSimple(productThirdSimple, productThirdSimple.getMeasure_unit());

                if (read.size()<=0){
                    msn.add("It does not recognize ID Product third, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = productThirdDAO.readCommons(formatoLongSql(id_product_third));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), productThirdDTO.getStateDTO());

                actualProductThirdSimple = read.get(0);

                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (productThirdDTO.getId_third() == null || productThirdDTO.getId_third()<1) {
                    productThirdDTO.setId_third(actualProductThirdSimple.getId_third());
                } else {
                    productThirdDTO.setId_third(formatoLongSql(productThirdDTO.getId_third()));
                }

                if (productThirdDTO.getStandard_price() == null) {
                    productThirdDTO.setStandard_price(actualProductThirdSimple.getStandard_price());
                } else {
                    productThirdDTO.setStandard_price(formatoDoubleSql(productThirdDTO.getStandard_price()));
                }
                if (productThirdDTO.getMin_price() == null) {
                    productThirdDTO.setMin_price(actualProductThirdSimple.getMin_price());
                } else {
                    productThirdDTO.setMin_price(formatoDoubleSql(productThirdDTO.getMin_price()));
                }

                if (productThirdDTO.getId_product() == null || productThirdDTO.getId_product()<1) {
                    productThirdDTO.setId_product(actualProductThirdSimple.getId_product());
                } else {
                    productThirdDTO.setId_product(formatoLongSql(productThirdDTO.getId_product()));
                }

                if (productThirdDTO.getId_measure_unit() == null || productThirdDTO.getId_measure_unit()<1) {
                    productThirdDTO.setId_measure_unit(actualProductThirdSimple.getMeasure_unit().getId_measure_unit());
                } else {
                    productThirdDTO.setId_measure_unit(formatoLongSql(productThirdDTO.getId_measure_unit()));
                }

                if (productThirdDTO.getId_attribute_list() == null || productThirdDTO.getId_attribute_list()<1) {
                    productThirdDTO.setId_attribute_list(actualProductThirdSimple.getId_attribute_list());
                } else {
                    productThirdDTO.setId_attribute_list(formatoLongSql(productThirdDTO.getId_attribute_list()));
                }
                if (productThirdDTO.getCode() == null || productThirdDTO.getCode().isEmpty()) {
                    productThirdDTO.setCode(actualProductThirdSimple.getCode());
                } else {
                    productThirdDTO.setCode(formatoStringSql(productThirdDTO.getCode()));
                }

                // Attribute update
                productThirdDAO.update(id_product_third,productThirdDTO);

                return Either.right(id_product_third);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
