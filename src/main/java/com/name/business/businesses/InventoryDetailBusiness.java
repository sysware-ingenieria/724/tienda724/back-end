package com.name.business.businesses;

import com.name.business.DAOs.InventoryDetailDAO;
import com.name.business.entities.*;
import com.name.business.representations.InventoryDetailDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.inventoryDetailSanitation;
import static com.name.business.sanitations.EntitySanitation.inventoryDetailSanitationSimple;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class InventoryDetailBusiness {

    private InventoryDetailDAO inventoryDetailDAO;
    private CommonStateBusiness commonStateBusiness;
    private ProductThirdBusiness productThirdBusiness;

    public InventoryDetailBusiness(InventoryDetailDAO inventoryDetailDAO, CommonStateBusiness commonStateBusiness, ProductThirdBusiness productThirdBusiness) {
        this.inventoryDetailDAO = inventoryDetailDAO;
        this.commonStateBusiness = commonStateBusiness;
        this.productThirdBusiness = productThirdBusiness;
    }

    public Either<IException, Long> createInventoryDetail(Long id_inventory, List<InventoryDetailDTO> detailDTOS) {
        Long  id_common_state=null;
        try {
            if (!validatorIDInvetary(id_inventory)){
                return Either.left(new BussinessException("It ID Inventory does not exist register on Inventory, probably it has bad"));
            }
            for (InventoryDetailDTO detailDTO: detailDTOS){
                if (productThirdBusiness.validatorID(detailDTO.getId_product_third())){

                    Either<IException, Long> commonStateEither = commonStateBusiness.createCommonState(detailDTO.getStateDTO());
                    if (commonStateEither.isRight()) {
                        id_common_state = commonStateEither.right().value();
                    }
                    inventoryDetailDAO.create(formatoLongSql(id_inventory),formatoLongSql(id_common_state),detailDTO);
                }
            }
           return Either.right(id_inventory);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    private Boolean validatorIDInvetary(Long id_inventory) {
        Integer validator = 0;
        if (id_inventory==null){
            return false;
        }
        try {
            validator = inventoryDetailDAO.getValidatorIDInventory(id_inventory);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param id_inventory
     * @param inventoryDetailDTO
     * @return
     */
    public Either<IException, Long> updateInventaryDetail(Long id_inventory, List<InventoryDetailDTO> inventoryDetailDTO) {
        List<String> msn = new ArrayList<>();
        List<InventoryDetailSimple> read=new ArrayList<>();
        CommonSimple idCommons = null;
        InventoryDetailSimple actualInventaryDetail=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Inventory Detail ||||||||||||||||| ");
            if (!validatorIDInvetary(id_inventory)){
                return Either.left(new BussinessException("It ID Inventory does not exist register on Inventory, probably it has bad"));
            }
            if ((id_inventory != null && id_inventory > 0)) {

                for (InventoryDetailDTO detailDTO:inventoryDetailDTO){
                    if (detailDTO.getId_inventory_detail()!=null && detailDTO.getId_inventory_detail()>0){

                        read = inventoryDetailDAO.readSimple(
                                new InventoryDetailSimple(
                                        detailDTO.getId_inventory_detail(),id_inventory,null,null,null,
                                        new CommonState(null,null,null,null)));

                        if (read.size()<=0){
                            msn.add("It ID Inventory Detail does not exist register on  Attribute Value, probably it has bad");

                        }else {
                            List<CommonSimple> readCommons = inventoryDetailDAO.readCommons(formatoLongSql(  detailDTO.getId_inventory_detail()),null);
                            if (readCommons.size()>0){
                                idCommons = readCommons.get(0);
                            }



                            // Common  State business update
                            commonStateBusiness.updateCommonState(idCommons.getId_common_state(), detailDTO.getStateDTO());

                            actualInventaryDetail = read.get(0);
                            // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                            if (detailDTO.getId_product_third() == null || detailDTO.getId_product_third()<1) {
                                detailDTO.setId_product_third(actualInventaryDetail.getId_product_third());
                            } else {
                                detailDTO.setId_product_third(formatoLongSql(detailDTO.getId_product_third()));
                            }

                            // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                            if (detailDTO.getQuantity() == null || detailDTO.getId_product_third()<0) {
                                detailDTO.setQuantity(actualInventaryDetail.getQuantity());
                            } else {
                                detailDTO.setQuantity(formatoIntegerSql(detailDTO.getQuantity()));
                            }

                            // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                            if (detailDTO.getCode() == null || detailDTO.getCode().isEmpty()) {
                                detailDTO.setCode(actualInventaryDetail.getCode());
                            } else {
                                detailDTO.setCode(formatoStringSql(detailDTO.getCode()));
                            }

                            // Inventory update
                            inventoryDetailDAO.update(detailDTO.getId_inventory_detail(),id_inventory,detailDTO);
                        }


                    }

                }
                return Either.right(id_inventory);

            } else {
                msn.add("It does not recognize ID Inventory Detail, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param inventoryDetailSimple
     *
     * @return
     */
    public Either<IException, List<InventoryDetailSimple>> getInventoryDetailSimple(InventoryDetailSimple inventoryDetailSimple) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Inventories Simple  ||||||||||||||||| ");
            InventoryDetailSimple inventoryDetailSimpleSanitation = inventoryDetailSanitationSimple(inventoryDetailSimple);
            return Either.right(inventoryDetailDAO.readSimple(inventoryDetailSimpleSanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param inventoryDetail
     *
     * @return
     */
    public Either<IException, List<InventoryDetail>> getInventoryDetails(InventoryDetail inventoryDetail) {
        List<String> msn = new ArrayList<>();
        try {

            System.out.println("|||||||||||| Starting consults Inventories Complete ||||||||||||||||| ");
            InventoryDetail inventoryDetailSanitation = inventoryDetailSanitation(inventoryDetail);

            return Either.right(inventoryDetailDAO.read(inventoryDetailSanitation.getDescription(),
                    inventoryDetailSanitation.getDescription().getMeasure_unit(),
                    inventoryDetailSanitation.getProduct(),
                    inventoryDetailSanitation.getDetail()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> deleteInventoryDetail(Long id_inventory_detail,Long id_inventory) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Inventory Detail ||||||||||||||||| ");
            if ((id_inventory != null && id_inventory > 0) || (id_inventory_detail!=null && id_inventory_detail>0)) {
                List<CommonSimple> readCommons = inventoryDetailDAO.readCommons(formatoLongSql(id_inventory_detail),formatoLongSql(id_inventory));
                if (readCommons.size()>0){
                    for (CommonSimple commonSimple: readCommons){
                        Either<IException, Long> deleteCommonStateEither = commonStateBusiness.deleteCommonState(commonSimple.getId_common_state());
                    }

                }



                return Either.right(id_inventory);
            } else {
                msn.add("It does not recognize ID  Inventory, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> discountInventaryDetail(Long id_inventory_detail, Object ob) {
        return null;
    }

    public Either<IException, Long> plusInventaryDetail(Long id_inventory_detail, Object ob) {
        return null;
    }
}
