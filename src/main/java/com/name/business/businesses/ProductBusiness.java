package com.name.business.businesses;

import com.name.business.DAOs.ProductDAO;
import com.name.business.entities.*;
import com.name.business.representations.ProductDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.productSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class ProductBusiness {
    private ProductDAO productDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;
    private CategoryBusiness categoryBusiness;

    public ProductBusiness(ProductDAO productDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness,
                           CategoryBusiness categoryBusiness) {
        this.productDAO = productDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
        this.categoryBusiness = categoryBusiness;
    }

    public Either<IException,Long> createProduct(ProductDTO productDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_product = null;
        Long id_common_state = null;
        System.out.println(productDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Product  ||||||||||||||||| ");
            if (productDTO!=null){

                //TODO validate the  id_category if exist on database a register with this id
                if (categoryBusiness.validatorID(productDTO.getId_category()).equals(false)){
                    msn.add("It ID Category does not exist register on  Category, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (productDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(productDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (productDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(productDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }



                productDAO.create(productDTO,formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_product = productDAO.getPkLast();

                return Either.right(id_product);
            }else{
                msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param product
     *
     * @return
     */
    public Either<IException, List<Product>> getProduct(Product product) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Product ||||||||||||||||| ");
            Product productSanitation = productSanitation(product);
            return Either.right(productDAO.read(productSanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = productDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param id_product
     * @return
     */
    public Either<IException, Long> deleteProduct(Long id_product) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Attribute  List ||||||||||||||||| ");
            if (id_product != null && id_product > 0) {
                List<CommonSimple> readCommons = productDAO.readCommons(formatoLongSql(id_product));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_product);
            } else {
                msn.add("It does not recognize ID Attribute List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_product
     * @param productDTO
     * @return
     */
    public Either<IException, Long> updateProduct(Long id_product, ProductDTO productDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Product actualProduct=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Product ||||||||||||||||| ");
            if ((id_product != null && id_product > 0)) {

                //TODO validate the  id attribute if exist on database a register with this id
                if (categoryBusiness.validatorID(productDTO.getId_category()).equals(false)){
                    msn.add("It ID Category does not exist register on  Category , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<Product> read = productDAO.read(
                        new Product(id_product, null,null,null,null,null,null,
                        new Common(null,null,null),
                        new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    msn.add("It does not recognize ID Product , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = productDAO.readCommons(formatoLongSql(id_product));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), productDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), productDTO.getStateDTO());

                actualProduct = read.get(0);
                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (productDTO.getId_category() == null || productDTO.getId_category()<1) {
                    productDTO.setId_category(actualProduct.getId_category());
                } else {
                    productDTO.setId_category(formatoLongSql(productDTO.getId_category()));
                }

                if (productDTO.getId_tax() == null || productDTO.getId_tax()<1) {
                    productDTO.setId_tax(actualProduct.getId_tax());
                } else {
                    productDTO.setId_tax(formatoLongSql(productDTO.getId_tax()));
                }

                if (productDTO.getStock() == null || productDTO.getStock()<1) {
                    productDTO.setStock(actualProduct.getStock());
                } else {
                    productDTO.setStock(formatoIntegerSql(productDTO.getStock()));
                }
                if (productDTO.getStock_min() == null || productDTO.getStock_min()<1) {
                    productDTO.setStock_min(actualProduct.getStock_min());
                } else {
                    productDTO.setStock_min(formatoIntegerSql(productDTO.getStock_min()));
                }

                if (productDTO.getImg_url() == null || productDTO.getImg_url().isEmpty()) {
                    productDTO.setImg_url(actualProduct.getImg_url());
                } else {
                    productDTO.setImg_url(formatoStringSql(productDTO.getImg_url()));
                }

                if (productDTO.getCode() == null || productDTO.getCode().isEmpty()) {
                    productDTO.setCode(actualProduct.getCode());
                } else {
                    productDTO.setCode(formatoStringSql(productDTO.getCode()));
                }

                // Attribute update
                productDAO.update(id_product,productDTO);

                return Either.right(id_product);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
