package com.name.business.businesses;

import com.name.business.DAOs.CommonStateDAO;
import com.name.business.entities.CommonState;
import com.name.business.representations.CommonStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.commonSanitation;
import static com.name.business.sanitations.EntitySanitation.commonStateSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class CommonStateBusiness {

    private CommonStateDAO commonStateDAO;

    public CommonStateBusiness(CommonStateDAO commonStateDAO) {
        this.commonStateDAO = commonStateDAO;
    }

    /**
     *
     * @param commonState
     *
     * @return
     */
    public Either<IException, List<CommonState>> getCommoStates(CommonState commonState) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Common ||||||||||||||||| ");
            return Either.right(commonStateDAO.read(commonStateSanitation(commonState)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
    public Either<IException,Long> createCommonState(CommonStateDTO commonStateDTO){
        List<String> msn = new ArrayList<>();
        Long id_common_state = null;
        System.out.println(commonStateDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Common State ||||||||||||||||| ");
            if (commonStateDTO!=null){

                //It validate the modify date, if null, set the creation date to the actual date
                if (commonStateDTO.getCreation_date() == null) {
                    commonStateDTO.setCreation_date(new Date());
                }

                //It validate the modify date, if null, set the modify date to the actual date
                if (commonStateDTO.getModify_date() == null) {
                    commonStateDTO.setModify_date(new Date());
                }

                //It validate the state, if null, set the state to 1
                if (commonStateDTO.getState() == null) {
                    commonStateDTO.setState(1);
                }
                commonStateDAO.create(commonStateDTO);

                id_common_state = commonStateDAO.getPkLast();
                return Either.right(id_common_state);
            }else{
                msn.add("It does not recognize Common State, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param id_common_state
     * @param commonStateDTO
     * @return
     */
    public Either<IException, Long> updateCommonState(Long id_common_state, CommonStateDTO commonStateDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Common State ||||||||||||||||| ");

            if (commonStateDTO != null && (id_common_state!=null && id_common_state>0)) {

                // Common update
                commonStateDAO.update(formatoLongSql(id_common_state),validatorCommonStateDTO(id_common_state,commonStateDTO));

                return Either.right(id_common_state);

            } else {
                msn.add("It does not recognize Common State, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param idCommon_State
     * @return
     */
    public Either<IException, Long> deleteCommonState(Long idCommon_State) {
        List<String> msn = new ArrayList<>();
        try {
            if (idCommon_State != 0) {
                msn.add("OK");
                System.out.println("|||||||||||| Starting CHANGE STATE DELETE Common State ||||||||||||||||| ");
                commonStateDAO.delete(formatoLongSql(idCommon_State), new Date());

                return Either.right(idCommon_State);
            } else {
                msn.add("It does not recognize ID Common State, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param commonState
     * @return
     */
    public Either<IException, Long> permanentDeleteCommonSate(CommonState commonState) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting PERMANENT DELETE Common State ||||||||||||||||| ");
            if (commonState != null) {
                commonStateDAO.permanentDelete(commonStateSanitation(commonState));
                return Either.right(commonState.getId_common_state());

            } else {
                msn.add("It does not recognize id_Common State, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private CommonStateDTO validatorCommonStateDTO(Long id_common_state, CommonStateDTO commonStateDTO) {
        try {
            CommonState currentCommonState=null;
            if(id_common_state != null && id_common_state != 0) {

                List<CommonState> commonList = commonStateDAO.read(new CommonState(id_common_state, null, null,null));
                if (commonList.size()>0){
                    currentCommonState = commonList.get(0);
                }

                if (commonStateDTO.getModify_date() == null) {
                    commonStateDTO.setModify_date(new Date());
                }

                if (commonStateDTO.getCreation_date() == null) {

                    commonStateDTO.setCreation_date(currentCommonState.getCreation_date());
                }

                if (commonStateDTO.getState()==null || commonStateDTO.getState()<0){
                    commonStateDTO.setState(currentCommonState.getState());
                }else{
                    commonStateDTO.setState(formatoIntegerSql(commonStateDTO.getState()));
                }

                return commonStateDTO;

            } else {

                return null;
            }
        }catch (Exception e){
            return  null;
        }
    }


}
