package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InventoryDetailDTO {
    private Long id_inventory_detail;
    private Long id_product_third;
    private Integer quantity;
    private String code;
    private CommonStateDTO stateDTO;

    @JsonCreator
    public InventoryDetailDTO(@JsonProperty("id_inventory_detail") Long id_inventory_detail,
            @JsonProperty("id_product_third") Long id_product_third,
                              @JsonProperty("quantity") Integer quantity,@JsonProperty("code") String code,
                              @JsonProperty("state") CommonStateDTO stateDTO) {


        this.id_inventory_detail=id_inventory_detail;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.code = code;
        this.stateDTO = stateDTO;
    }



    public Long getId_inventory_detail() {
        return id_inventory_detail;
    }

    public void setId_inventory_detail(Long id_inventory_detail) {
        this.id_inventory_detail = id_inventory_detail;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
