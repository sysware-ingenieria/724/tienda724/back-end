package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductThirdDTO {
    private Long id_third;
    private Double min_price;
    private Double standard_price;
    private Long id_product;
    private Long id_measure_unit;
    private CommonStateDTO stateDTO;
    private Long id_attribute_list;
    private String code;
    private Long id_category_third;
    private String location;

    @JsonCreator
    public ProductThirdDTO(
            @JsonProperty("id_third") Long id_third,@JsonProperty("standard_price") Double standard_price,
            @JsonProperty("min_price") Double min_price,
            @JsonProperty("id_product") Long id_product,@JsonProperty("id_measure_unit") Long id_measure_unit,
            @JsonProperty("state") CommonStateDTO stateDTO,@JsonProperty("id_attribute_list")  Long id_attribute_list,
            @JsonProperty("code") String code,
            @JsonProperty("id_category_third") Long id_category_third,
            @JsonProperty("location") String location) {

        this.id_third = id_third;
        this.standard_price = standard_price;
        this.min_price=min_price;
        this.id_product = id_product;
        this.id_measure_unit = id_measure_unit;
        this.stateDTO = stateDTO;
        this.id_attribute_list = id_attribute_list;
        this.code = code;
        this.id_category_third = id_category_third;
        this.location=location;
    }

    public Long getId_category_third() {
        return id_category_third;
    }

    public void setId_category_third(Long id_category_third) {
        this.id_category_third = id_category_third;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getMin_price() {
        return min_price;
    }

    public void setMin_price(Double min_price) {
        this.min_price = min_price;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Double getStandard_price() {
        return standard_price;
    }

    public void setStandard_price(Double standard_price) {
        this.standard_price = standard_price;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_measure_unit() {
        return id_measure_unit;
    }

    public void setId_measure_unit(Long id_measure_unit) {
        this.id_measure_unit = id_measure_unit;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
