package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributeDTO {
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;


    @JsonCreator
    public AttributeDTO(@JsonProperty("common") CommonDTO commonDTO,
                        @JsonProperty("state") CommonStateDTO stateDTO) {
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
