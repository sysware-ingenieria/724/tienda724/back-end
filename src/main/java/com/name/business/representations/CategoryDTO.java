package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryDTO {
    private String img_url;
    private Long id_category_father;
    private Long id_third_category;
    private CommonDTO commonDTO;
    private CommonStateDTO commonStateDTO;


    @JsonCreator
    public CategoryDTO(@JsonProperty ("img_url") String img_url,
                           @JsonProperty ("id_category_father") Long id_category_father,
                       @JsonProperty ("id_third_category") Long id_third_category,
                       @JsonProperty ("common")CommonDTO commonDTO,
                       @JsonProperty ("state") CommonStateDTO commonStateDTO) {

        this.img_url = img_url;
        this.id_category_father = id_category_father;
        this.id_third_category = id_third_category;
        this.commonDTO = commonDTO;
        this.commonStateDTO = commonStateDTO;
    }

    public Long getId_third_category() {
        return id_third_category;
    }

    public void setId_third_category(Long id_third_category) {
        this.id_third_category = id_third_category;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Long getId_category_father() {
        return id_category_father;
    }

    public void setId_category_father(Long id_category_father) {
        this.id_category_father = id_category_father;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getCommonStateDTO() {
        return commonStateDTO;
    }

    public void setCommonStateDTO(CommonStateDTO commonStateDTO) {
        this.commonStateDTO = commonStateDTO;
    }
}

