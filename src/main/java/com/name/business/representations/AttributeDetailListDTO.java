package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributeDetailListDTO {

    private Long id_attribute_value;
    private CommonStateDTO stateDTO;

    @JsonCreator
    public AttributeDetailListDTO(@JsonProperty("id_attribute_value") Long id_attribute_value,
                                  @JsonProperty("state") CommonStateDTO stateDTO) {

        this.id_attribute_value = id_attribute_value;
        this.stateDTO = stateDTO;
    }


    public Long getId_attribute_value() {
        return id_attribute_value;
    }

    public void setId_attribute_value(Long id_attribute_value) {
        this.id_attribute_value = id_attribute_value;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
