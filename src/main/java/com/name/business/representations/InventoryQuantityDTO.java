package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class
InventoryQuantityDTO {
    private Long id_inventory_detail;
    private Long id_product_third;
    private Integer quantity;
    private String code;

    @JsonCreator
    public InventoryQuantityDTO(@JsonProperty("id_inventory_detail") Long id_inventory_detail,
                                @JsonProperty("id_product_third") Long id_product_third,
                                @JsonProperty("quantity") Integer quantity,
                                @JsonProperty("code") String code) {
        this.id_inventory_detail = id_inventory_detail;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.code = code;
    }

    public Long getId_inventory_detail() {
        return id_inventory_detail;
    }

    public void setId_inventory_detail(Long id_inventory_detail) {
        this.id_inventory_detail = id_inventory_detail;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
