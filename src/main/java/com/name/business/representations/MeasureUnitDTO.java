package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MeasureUnitDTO {
    private Long id_measure_unit_father;
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;



    @JsonCreator
    public MeasureUnitDTO(@JsonProperty("id_measure_unit_father") Long id_measure_unit_father,
                          @JsonProperty("common") CommonDTO commonDTO,
                            @JsonProperty("state") CommonStateDTO stateDTO) {
        this.id_measure_unit_father = id_measure_unit_father;
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
    }

    public Long getId_measure_unit_father() {
        return id_measure_unit_father;
    }

    public void setId_measure_unit_father(Long id_measure_unit_father) {
        this.id_measure_unit_father = id_measure_unit_father;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
