package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PriceListDTO {

    private  Long id_product_third;
    private Double  standard_price;
    private Double  min_price;
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;


    @JsonCreator
    public PriceListDTO(@JsonProperty("id_product_third") Long id_product_third,@JsonProperty("standard_price") Double standard_price,
                        @JsonProperty("min_price") Double min_price,@JsonProperty("common") CommonDTO commonDTO,
                        @JsonProperty("state") CommonStateDTO stateDTO) {
        this.id_product_third = id_product_third;
        this.standard_price = standard_price;
        this.min_price = min_price;
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Double getStandard_price() {
        return standard_price;
    }

    public void setStandard_price(Double standard_price) {
        this.standard_price = standard_price;
    }

    public Double getMin_price() {
        return min_price;
    }

    public void setMin_price(Double min_price) {
        this.min_price = min_price;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
