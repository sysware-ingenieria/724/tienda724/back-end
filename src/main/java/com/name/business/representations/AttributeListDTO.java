package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributeListDTO {
    private CommonStateDTO stateDTO;


    @JsonCreator
    public AttributeListDTO(@JsonProperty("state") CommonStateDTO stateDTO) {

        this.stateDTO = stateDTO;

    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
