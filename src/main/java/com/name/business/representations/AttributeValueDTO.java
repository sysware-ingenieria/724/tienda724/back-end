package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributeValueDTO {

    private Long id_attribute;
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;


    @JsonCreator
    public AttributeValueDTO(@JsonProperty("id_attribute") Long id_attribute, @JsonProperty("common") CommonDTO commonDTO,
                             @JsonProperty("state") CommonStateDTO stateDTO) {
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
        this.id_attribute=id_attribute;
    }


    public Long getId_attribute() {
        return id_attribute;
    }

    public void setId_attribute(Long id_attribute) {
        this.id_attribute = id_attribute;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
