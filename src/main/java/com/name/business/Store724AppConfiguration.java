package com.name.business;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
/**
 * La clase Store724AppConfiguration Se establece los parámetros de  configuración que se requieren
 * con la base de datos(MySql)
 * */
public class Store724AppConfiguration extends Configuration {


    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    @NotNull
    private Store724DatabaseConfiguration databaseConfiguration;


    public DataSourceFactory getDataSourceFactory(){
        return database;
    }
    public Store724DatabaseConfiguration getDatabaseConfiguration(){
        return databaseConfiguration;
    }

}
